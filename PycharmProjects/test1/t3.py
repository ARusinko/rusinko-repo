import sys
import pyodbc

#
#		MAIN Code
#
def main(argv):

#   Get complete list of partnos and mfg
    CONN1 = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=PricingAnalytics;'
                       'Trusted_Connection=yes;')

    SQLcursor = CONN1.cursor()

    # delete table if needed
#    SQLcursor.execute("""DROP TABLE generic;""")

    # Create new generic table
    sql_command = """
    CREATE TABLE dbo.Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255) ); """
    SQLcursor.execute(sql_command)
    CONN1.commit()

    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)
