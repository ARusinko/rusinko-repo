import sys
import pyodbc

#
#		MAIN Code
#
def main(argv):

#   Get complete list of partnos and mfg
    CONN1 = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=PricingAnalytics;'
                       'Trusted_Connection=yes;')

    SQLcursor = CONN1.cursor()

    try:
    # delete table if needed
        SQLcursor.execute("""DROP TABLE dbo.Persons;""")
    except pyodbc.ProgrammingError:
        pass

    lastname  = ["Rusinko",'',"Dyer","Markwalter"]
    firstname = ['Andy', 'Kathy', 'Jessica', 'Katharine' ]
    locales   = ['2502 Mandy Way','2502 Mandy Way','2502 Mandy Way','18 Park Place Circle']
    cities    = ['Arlington','Arlington','Arlington','Augusta']

    # Create new generic table
    sql = """
    CREATE TABLE dbo.Persons (
    PersonID decimal(12,5),
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255) ); """

    SQLcursor.execute(sql)


    newdata = []
    for ix, lname in enumerate(lastname):
        PID = None
        newtup = (PID, lname, firstname[ix], locales[ix], cities[ix])
        newdata.append(newtup)
    print(newdata)


    sql_command = """insert into dbo.Persons(PersonID, LastName, FirstName, Address, City)
      VALUES(?,?,?,?,?)"""

    number_of_rows = SQLcursor.executemany(sql_command, newdata)

    CONN1.commit()
    CONN1.close()

    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)


