SELECT *
FROM
(
SELECT  DISTINCT 'www.ttiinc.com' AS Domain,
mfg,
[Mfg Part Number] AS [Part Number],
CONVERT(DATE, (GETDATE() -1)) AS [PriceDate],
[On Hand],
[Min],
[RESALE QTY 1],
[INET RESALE PRICE 1],
[RESALE QTY 2],
[INET RESALE PRICE 2],
[RESALE QTY 3],
[INET RESALE PRICE 3],
[RESALE QTY 4],
[INET RESALE PRICE 4],
[RESALE QTY 5],
[INET RESALE PRICE 5],
[RESALE QTY 6],
[INET RESALE PRICE 6],
[RESALE QTY 7],
[INET RESALE PRICE 7],
[RESALE QTY 8],
[INET RESALE PRICE 8]
FROM [Product].[dbo].[vwPartDI_NDC] W where Whse in ('TX','02') AND MFG NOT IN ('AAA','DUM') AND [Mfg Part Number] != ''
AND [Mfg Part Number] NOT LIKE '?%' AND [Mfg Part Number] NOT LIKE '**%' AND [Mfg Part Number] NOT LIKE '$%' -- AND [Part Number] ='G5LE-14-DC12'
) A

UNION

SELECT * FROM
(
SELECT
      [Domain]
      ,[VendorCode] as[mfg]
      ,SUBSTRING([TTIPartNumber], 4, LEN([TTIPartNumber])) as [Part Number]
      ,[PriceDate]
      ,[ATS] as [On Hand]
      ,[QB1] as [Min]
      ,[QB1] as [RESALE QTY 1]
      ,[PB1] as [INET RESALE PRICE 1]
      ,[QB2] as [RESALE QTY 2]
      ,[PB2] as [INET RESALE PRICE 2]
      ,[QB3] as [RESALE QTY 3]
      ,[PB3] as [INET RESALE PRICE 3]
      ,[QB4] as [RESALE QTY 4]
      ,[PB4] as [INET RESALE PRICE 4]
      ,[QB5] as [RESALE QTY 5]
      ,[PB5] as [INET RESALE PRICE 5]
      ,[QB6] as [RESALE QTY 6]
      ,[PB6] as [INET RESALE PRICE 6]
      ,[QB7] as [RESALE QTY 7]
      ,[PB7] as [INET RESALE PRICE 7]
      ,[QB8] as [RESALE QTY 8]
      ,[PB8] as [INET RESALE PRICE 8]
  FROM [Mouser].[dbo].[CompetitorProducts_TTI] where TTIPartNumber is not NULL   -- AND [TTIPartNumber] ='OMRG5LE-14-DC12'
  ) B
  ORDER BY [Part Number], [Mfg], [Domain], [PriceDate];