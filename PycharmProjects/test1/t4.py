import sys
import pyodbc

CONNX = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=Product;'
                       'Trusted_Connection=yes;')

cursor = CONNX.cursor()


SQLquery = """ SELECT distinct mfg, [Part Number], [Min], \
                [RESALE QTY 1],[INET RESALE PRICE 1], [RESALE QTY 2], [INET RESALE PRICE 2],  \
                [RESALE QTY 3],  [INET RESALE PRICE 3], [RESALE QTY 4],[INET RESALE PRICE 4], \
                [RESALE QTY 5],[INET RESALE PRICE 5],  [RESALE QTY 6], [INET RESALE PRICE 6], \ 
                [RESALE QTY 7], [INET RESALE PRICE 7], [RESALE QTY 8], [INET RESALE PRICE 8]  \
            FROM [Product].[dbo].[vwPartDI_NDC] where Whse in ('TX','02') AND MFG NOT IN ('AAA','DUM') """

cursor.execute(SQLquery)          # SQL Query Execution
for row in cursor:
     print("row = %r" % (row,))
