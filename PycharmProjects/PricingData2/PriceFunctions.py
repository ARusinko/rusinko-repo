#---------------------------------------------------------------------------------------------------------------
#
#           PricingFunctions v1.0: Functions used to manipulate data used in Scoring Functions.
#
#                                   A. Rusinko  (11/30/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import numpy as np

__author__ = 'ARusinko'

#
#   Create the Quantity/Price Matrix, QPMat
#
# Generate a Quantity/Price Matix and return (QPMat)
def BuildQPMatrix(PDict, PBQ):
    lenPBQ    = len(PBQ)                    # lenPBQ -- Length of Price Break Quantity Vector
    lenVal    = len(PDict)                  # lenVal -- Length of Pricing Dictionary

    # Create default Quantity/Price array of all NEGATIVE ones
    QPMat = -1.0 * np.ones((lenPBQ, lenVal))

    ATSList = []
    MaxTier = []
    # Add Price Data
    for ix,dk in enumerate(PDict):
        RList = PDict[dk]
        ATSList.append(RList[4])            # Create of list of ATS by vendor

        if len(RList)>6:
            (Q,P) = RList[-1]               # Max quantity break
        else:
            Q = 0
        MaxTier.append(Q)

        for (Q,P) in RList[6:]:             # Matrix TRANSPOSE
            jy = PBQ.index(Q)
            QPMat[jy,ix] = P

    # Fill in price matrix by using lower break if available
    for j in range(lenVal):
        ATS = ATSList[j]                    # ATS for check of exceeding price breaks
        MxQ = MaxTier[j]

        xlast = -1.0
        for i in range(lenPBQ):
            if (PBQ[i]>ATS):
                break                           # Greater than available to sell, break
            else:
                if (PBQ[i]>MxQ): break          # Break if bigger than MaxTier

            if QPMat[i,j] > 0.00000001:         # Store Price value
                xlast = QPMat[i,j]
            elif QPMat[i,j] < 0.0:
                QPMat[i, j] = xlast

    # Change ALL -1.0 to np.NAN
    for j in range(lenVal):
        for i in range(lenPBQ):
            if QPMat[i,j] < 0.0: QPMat[i,j] = np.NaN

    return QPMat                                # Return Quantity/Price Matrix

#
#   Other useful functions needed to process data
#
# Canonacolize Pricing Dictionary by Distributor and PackageSize
def CanonPriceDictionay(PDict,ZeroOK = False):
    MList = []
    for dk in PDict:
        RList = PDict[dk]
        ptup  = (RList[2],RList[0],RList[5],RList[4],dk)
        MList.append(ptup)
    MList.sort(reverse=True)

    NList = []
    for (a,b,c,d,e) in MList:
        if ZeroOK == False and d==0:
            del PDict[e]
            continue
        tup = (b,c)
        if tup in NList:
            del PDict[e]
        else:
            NList.append(tup)
    return PDict

# Get combined list of all break points
def GetPriceBreakPoints(PDict):
    PriceBreakQuantity = []
    for p in PDict:
        RList = PDict[p]
        for (q,p) in RList[6:]:
            if q in PriceBreakQuantity:
                continue
            else:
                PriceBreakQuantity.append(q)
    PriceBreakQuantity.sort()
    return PriceBreakQuantity

# Remove Price Break tuples if they exceed ATS, Available To Sell
def RemoveHighPriceBreaks(PDict,ATSOK=False):
    for dk in PDict:
        RList = PDict[dk]
        ATS = RList[4]
        DictRemove = []
        NList      = []
        for i in range(6,len(RList)):
            (Q,P) = RList[i]
            if Q > ATS and ATSOK==False: NList.append((Q,P))
        for nn in NList:
            RList.remove(nn)

        if len(RList)==6: DictRemove.append(dk)         # Remove entries with NO tiers left

    for dk in DictRemove:
        del PDict[dk]
    return PDict                            # Return Dictionary -- PrDict

#
#   Create column names based on Competitor and Setup
#

# Create a vector of column names, Supplier(StandardPackage)
def GetColumnNames(PDict):
    CName    = []
    NumbComp = 0
    for dk in PDict:
        RList = PDict[dk]
        if RList[0] != "TTI": NumbComp += 1
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        CName.append(ColName)
    return (NumbComp,CName)                            # Return list of column names

# Create a dictionary (competitors,index)
def GetNameIndex(PDict):
    Idx = {}
    for dk in PDict:
        RList = PDict[dk]
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        Idx[ColName] = dk
    return Idx

# Calculate Average and Maximum ATS for all vendors
def getAveCompATS(PDict, CIdx):
    sum    = 0
    nct    = 0
    maxval = -99999999
    for cname in CIdx:
        if "TTI" in cname or "MOU" in cname: continue       # Skip TTI or Mou
        RList = PDict[CIdx[cname]]
        sum  += RList[4]
        nct  += 1
        if RList[4]>maxval: maxval = RList[4]               # Max ATS
    if  maxval<0.001: maxval = 0
    if nct>0:
        average = float(sum)/float(nct)
    else:
        average = 0.0
    return (int(average),maxval)

#
#   Data frame manipulations
#
#   Get Competitor with minimum price and the last update of that price
def GetMinCompetitors(PDict, dfr):
    (nrows,ncols) = dfr.shape
    dfr['MinPriceComp']  = [np.NaN]*nrows
    dfr['LastPriceDate'] = [np.NaN]*nrows
    LastValue = np.NaN
    LastDate  = np.NaN
    for index, row in dfr.iterrows():                   # Update dataframe to include Competitor with MinPrice, MinPriceComp
        if np.isnan(row['CompMin']): continue           # as well as the last update for that price
        for dk in PDict:
            pdv = PDict[dk]
            if pdv[0] == "TTI" or pdv[0] == "MOU": continue

            if (index, row['CompMin']) in pdv:              # Find competitor with min value/date
                dfr.loc[index, 'MinPriceComp']  = pdv[0]
                LastValue = pdv[0]
                LastDate = pdv[2]
                break

        dfr.loc[index, 'MinPriceComp'] = LastValue
        dfr.loc[index,'LastPriceDate'] = LastDate
    pass

# Look for ALL TTI Price Tiers
def GetTTI_Tiers(prow,dfr):
    (nrows,ncols) = dfr.shape
    STP   = prow[5]
    PList = []
    for (q, p) in prow[6:]:
        if q not in PList: PList.append(q)
    PList.sort()

    dfr['TTI_Index'] = [0] * nrows
    dfr['TTI_Break'] = ['N'] * nrows
    ii = 1
    for index, row in dfr.iterrows():
        if index in PList:
            dfr.loc[index, 'TTI_Index'] = ii
            dfr.loc[index, 'TTI_Break'] = 'Y'
            ii += 1
        elif index % STP == 0:
            dfr.loc[index, 'TTI_Break'] = 'P'
    pass