#---------------------------------------------------------------------------------------------------------------
#
#           PricingData: Gather TTI pricing data from SQL Server and construct a matrix of
#                        quantity/price break information. Use pandas dataframe for cleaner
#                        manipulation of data.
#
#                                   A. Rusinko  (11/30/2017) v2.0
#---------------------------------------------------------------------------------------------------------------
import sys
import pyodbc
import numpy as np
import pandas as pd
import time
from PriceFunctions import *
from ScoringFunctions import *
from IOFunctions import *

__author__ = 'ARusinko'

MINBREAK = 3                                                              # STOP after exceeding MINBREAK

DEBUGLevel = "N"                                                           # Levels = 'N,D,Q,M,C'

#   Dictionary for test cases --- mpno and mfr
TTIDict = {'RK73H1ETTP4641F':'KOA', 'T495X226K025ATE225':'KEM', '0002066102':'MOL',
            'ERJ8GEYJ181V':'PAN', 'ANT7020LL05R2400A':'YAG', 'MLSH701M075JK0C':'CDE',
            '0000109163':'GLE', '00015875':'ACO', '00111920202':'PCD',
            '518148321':'TYC', 'TNPW060310K9BEEA':'DAL',
            '0830000083':'MOL', '0002061103':'MOL','0002092133':'MOL', 'SM20ML1TK6':'SOU',
            '14803190':'TYC', '14899511':'TYC',
            '009155004201006':'AVX',  'NPC1210015A1S':'AAS', '353536526':'TYC',
            '1.5SMC400A':'BOU', '00021900':'ACO', '0002066100':'MOL', '0011400123': 'MOL',
            'B3FS4052P':'OMR','Y6408201V1534300':'DEU', 'ACJSMHDE':'AAL', 'UVR2CR47MED1TD':'NIC',
            '#A915AY100M=P3':'MUR', '000000000004100':'AIR',
            'TLP2366(V4TPL,E':'TOS', 'FS5,5X1': 'EPC'}

def ConvertNan2Null(val):
    vval = None
    if not np.isnan(val): vval = val
    return vval


def QuickCompare(x,y):
    val = 0
    if np.isnan(x) or np.isnan(y): return val
    if x<=0.000001 or y<=0.000001: return val
    if x>y: val =  1
    if x<y: val = -1
    return val

def CaclulateComparisons(jj, CNames, Competitors, Prices, QPMat, CSPList):
    (nrows, ncols) = QPMat.shape
    CompList=[0]*9

    for ix,cn in enumerate(CNames):
        dname = cn[0:3]
        if 'TTI' in dname: continue
        xidx = Competitors.index(dname)

        x = Prices[jj]
        y = QPMat[jj,ix]
        v = QuickCompare(x,y)
#        print(jj, cn,ix,x,y,v)
        if CompList[xidx] == 0: CompList[xidx] = v

    (Q,P) = CSPList[jj]
    v = QuickCompare(x, P)
    xidx  = Competitors.index('CSP')
    CompList[xidx] = v
    return CompList


#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

    Competitors = ['CSP']
    for tk in SCodes:
        if 'TTI' in SCodes[tk]: continue
        Competitors.append(SCodes[tk])
    Competitors.sort()
#    print(Competitors)

#   Output file
    try:
        f = open('./PriceComparisonTest.csv','w')
    except PermissionError:
        print("Can't open output csv file..... quitting")
        sys.exit()

#   Write out header row for csv file.
    f.write("Index, Break_Index, TTI_Index,VendorCode,ManufPartNum,ATS,StdPackage,Quantity,TTI,PriceUpdate,CSPrice,Mouser," +
            "CompAve,CompMin,CompMax,CompRange,CompAveATS,"+"CompMaxATS,MinVendor,LastUpdate,Score1,Score2,Allocation," +
            'ARW0,AVN0,CSP0,DKY0,FUT0,HEI0,MOU0,NRK0,PEI0,ARW1,AVN1,CSP1,DKY1,FUT1,HEI1,MOU1,NRK1,PEI1\n')

#   Get complete list of partnos and mfg
    CONN1 = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=PricingAnalytics;'
                       'Trusted_Connection=yes;')
    PartsList = CONN1.cursor()
    SQLquery = """ SELECT DISTINCT [MfrPartNumberCleaned], [mfg]
            FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] """
    PartsList.execute(SQLquery)

    #   Make new connection to database to write data
    CONN2 = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')
    SQLcursor = CONN2.cursor()

    # delete table if needed
#    try:
#        SQLcursor.execute("""DROP TABLE dbo.PriceComparisonTest;""")
#    except pyodbc.ProgrammingError:
#        pass

    # SQL Insert command
    sql_command = """insert into dbo.PriceComparisonTest(
        BreakCounter,TTI_Index,TTI_Break,Vendor,MFRPartNumber,
        TTI_ATS,StdPackage,MinQuantity,TTI_Price,
        PriceUpdate,CustSpecPrice,MouserPrice,CompAvePrice,
        CompMinPrice,CompMaxPrice,CompPriceRange,CompAveATS,
        CompMaxATS,MinPriceComp,LastPriceDate,PriceScore,
        InventoryScore,AllocationScore,
        TTIvARW, TTIvAVN, TTIvCSP, TTIvDKY,
        TTIvFUT, TTIvHEI, TTIvMOU, TTIvNRK, TTIvPEI,
        UPDvARW, UPDvAVN, UPDvCSP, UPDvDKY,
        UPDvFUT, UPDvHEI, UPDvMOU, UPDvNRK, UPDvPEI) 
        VALUES(?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?, ?)"""

    # Create NEW Price Comparison table
    sql = """
        CREATE TABLE dbo.PriceComparisonTest(
        BreakCounter int,
        TTI_Index int,
        TTI_Break varchar(1),
        Vendor varchar(3),
        MFRPartNumber varchar(50),
        TTI_ATS bigint,
        StdPackage int,
        MinQuantity int,
        TTI_Price decimal(12,4),
        PriceUpdate decimal(12,4),
        CustSpecPrice decimal(12,4),
        MouserPrice decimal(12,4),
        CompAvePrice decimal(12,4),
        CompMinPrice decimal(12,4),
        CompMaxPrice decimal(12,4),
        CompPriceRange decimal(12,4),
        CompAveATS decimal(12,4),
        CompMaxATS bigint,
        MinPriceComp varchar(3),
        LastPriceDate date,
        PriceScore decimal(12,4),
        InventoryScore decimal(12,4),
        AllocationScore int,
        TTIvARW int, TTIvAVN int, TTIvCSP int, TTIvDKY int,
        TTIvFUT int, TTIvHEI int, TTIvMOU int, TTIvNRK int, TTIvPEI int,
        UPDvARW int, UPDvAVN int, UPDvCSP int, UPDvDKY int,
        UPDvFUT int, UPDvHEI int, UPDvMOU int, UPDvNRK int, UPDvPEI int ); """

#    SQLcursor.execute(sql)
#    CONN2.commit()

#   New connection needed to retrieve data
    CONNX = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')
    icnt = 0
    for row in PartsList:
        if icnt==0: break
        mfrpartno = row[0]                          # Find part by Mfg Part Number and Mfg
        mfg       = row[1]

    for mfrpartno in TTIDict:                       # For testing purposes on specific cases
        mfg  = TTIDict[mfrpartno]
        mpno = mfrpartno
        print("\n",mfrpartno, mfg)

#       Record count
        icnt+=1
        if icnt > MINBREAK: break

        if icnt%1000==0:
            CONN2.commit()
            end = time.time()
            timed = '{:.1f}'.format(end - start)
            print(icnt, mfrpartno,timed)

#       SKIP if TTIMoveCode=="E" or SOAI = 'RF' or 'RM'
        (MoveCode, SOAI) = SQL_GetMCData(CONNX, mfg, mpno)
        if MoveCode=="E" or SOAI=="RF" or SOAI=="RM": continue

#       Get a dictionary containing lists of data by MnfPartNo and minimum ATS
#       minATS > 0 exlcudes any record with no stock on hand.
        PriceDict = SQL_GetPriceData(CONNX, mfrpartno, mfg, 1)              # TTI ATS must be at least 1
        if len(PriceDict)==0: continue

        mpno = 'NA'                                                         # Get mfr part no
        for pr in PriceDict:
            if "D" in DEBUGLevel: print (pr,PriceDict[pr])
            if "TTI" in PriceDict[pr][0]: mpno = PriceDict[pr][1][3:]

        if "," in mpno: continue  # CODE AROUND Comma in ManPartNo, SKIP for now

#       Keep only the most current entries in Pricing Dictionary, by Distributor and PackageSize
        PriceDict = CanonPriceDictionay(PriceDict,True)
        if "D" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Remove Price Breaks when insufficient quantity on hand.
#       ZeroOK -- if True, return ALL price breaks found
        PriceDict = RemoveHighPriceBreaks(PriceDict,False)
        if len(PriceDict)==0: continue

        if "Q" in DEBUGLevel or "C" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Get unique, sorted list of price breaks
        PBQ = GetPriceBreakPoints(PriceDict)
        NumPriceBreaks = len(PBQ)
        if NumPriceBreaks == 0: continue                                                      # Done, if no data found
        if "Q" in DEBUGLevel  or "C" in DEBUGLevel:  print(NumPriceBreaks, PBQ)               # Echo list

#       Create Column Names
        (NumbVendors,CName)  = GetColumnNames(PriceDict)                                # Make a vector of column names
        if NumbVendors==0: continue                                                     # No entries, skip
        if "Q" in DEBUGLevel  or "C" in DEBUGLevel: print(NumbVendors, CName)
        PDictIdx = GetNameIndex(PriceDict)                                              # Get PriceDict index per vendor/setup
        (CAveATS, CMaxATS) = getAveCompATS(PriceDict, PDictIdx)                         # Get Ave/Max Competitor ATS

#       Build Price Break Matrix and create a dataframe from it
        QPMat =  BuildQPMatrix(PriceDict, PBQ)
        if "M" in DEBUGLevel or "C" in DEBUGLevel: print(QPMat)
        df = pd.DataFrame(QPMat, columns=CName)
        df['MinQuantity'] = PBQ
        df.set_index('MinQuantity', inplace=True)                           # Set row index to price breaks

        TTIdf   = df.filter(regex='TTI')                                    # Create TTI dataframe
        TTIList = list(TTIdf)
        if len(TTIList)==0:    continue                                     # SKIP, if no TTI data

        for tti in TTIList:     df = df.drop(tti, 1)                        # Remove TTI columns from data matrix

        (nrows, ncols) = df.shape
        results = df.filter(regex='MOU')                                    # Create RESULTS Dataframe
        MOUSER  = list(df.filter(regex='MOU'))
        if len(MOUSER) >0:                                                  # Handle Mouser data
            df      = df.drop(MOUSER,1)
            results.rename(columns={MOUSER[0]: 'MouserPrice'}, inplace=True)
        else:
            results['MouserPrice'] = [np.NaN] * nrows

#       Add ancillary data
        results["BreakCounter"] = range(1, nrows + 1)
        results['MinQ'] = PBQ
        results['Mfr']  = [mfg] *nrows
        results['MPNo'] = [mpno]*nrows
        results['CompAveATS'] = [CAveATS]*nrows
        results['CompMaxATS'] = [CMaxATS]*nrows

#       Customer Specific Pricing
        CSPList = []
        CSPList = SQL_CustSpecPricing(CONNX, mpno, mfg, PBQ)
        CSPList.sort()
        CSPresults = []
        CSPbreak = np.nan
        for (Q, P) in CSPList:
            if Q in PBQ: CSPbreak = P
            CSPresults.append(CSPbreak)
        results['CSP'] = CSPresults

#       Get Allocation attribute for part, mpno
        Allocation = SQL_GetAllocData(CONNX, mfg, mpno)
        results['AllocationScore'] = [Allocation] * nrows

#       Add Stats per price break
        results['CompAve'] = df.mean(axis=1,skipna=True)                # Calculate by column, skipping any NaN
        results['CompMin'] = df.min(axis=1,skipna=True)
        results['CompMax'] = df.max(axis=1,skipna=True)

#       Add MinCompet and Date to results
        GetMinCompetitors(PriceDict, results)

#       Get quantity differences between breaks
        GetQuantityBetweenBreaks(results)
#
#       Iterate over each TTI setup. Add any other columns dependent on TTI setup.
#
        for column in TTIdf.columns:
            results['TTI_Price'] = TTIdf[column]                    # Add TTI prices for current setup
            dk  = PDictIdx[column]
            row = PriceDict[dk]

            GetTTI_Tiers(row, results)                              # Make list of TTI specific price tiers

            results['TTI_ATS']    = [row[4]]*nrows                  # Add TTI stock
            results['StdPackage'] = [row[5]]*nrows                  # Add TTI standard package

            CalcInvenScore(results, PriceDict)                      # Calcuate Inventory Score

            X1 = 9.0                    # Default settings -- No Allocation -- Drive towards CompMin
            X2 = 1.0
            if Allocation==1:           # Limited Supply -- Allocation=T -- Drive towards CompMean
                X1 = 1.0
                X2 = 3.0
            CalcPriceScore(X1,X2,results)                           # Calculate Pricing Score
            MakeTierScores(results)                                 # Recaclulated PriceScore based on tiers
            CalcNewPrice(results)                                   # Calculate an updated price

            print(results[['TTI_Index', 'TTI_Price','CompAve', 'OriPriceScore', 'WgtPriceScore']])

    #   CLOSE files and connections
    f.close()
    CONN1.close()
    CONNX.close()
    CONN2.close()

#   Echo processing time
    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt-1,"Processed ---- Elasped:",timed,'s')
    pass


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)


