#---------------------------------------------------------------------------------------------------------------
#
#           IOFunctions v1.0: Input/Output functions used by pricing model code.
#
#                                   A. Rusinko  (11/29/2017)
#---------------------------------------------------------------------------------------------------------------

import numpy as np

__author__ = 'ARusinko'

SCodes = {
    'www.ttiinc.com': 'TTI', 'www.peigenesis.com': 'PEI', 'www.newark.com': 'NRK',
    'www.futureelectronics.com': 'FUT', 'www.digikey.com': 'DKY', 'www.arrow.com': 'ARW',
    'avnetexpress.avnet.com': 'AVN', 'estore.heilind.com': 'HEI', 'www.mouser.com': 'MOU'}


#
#   SQL SERVER calls to fetch data
#

# SQL_GetCompPriceData By Manufacturer Part Number, reformat and return dictionary of relevant data
def SQL_GetPriceData(CONNX, TestPartNum, Manufacturer, minATS=0 ):
    global SCodes
    try:
        cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: PRICING_COMBINE
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] \
                WHERE [MfrPartNumberCleaned] = ? And [Mfg] = ? And [ATS] >= ? """

    cursor.execute(SQLquery,(TestPartNum, Manufacturer, minATS))       # SQL Query Execution

    PrDict = {}
    ix = 0
    for row in cursor:
#        print(row)
        RList  = []                                     # Create List for each record
        RList.append(SCodes[row[0]])                    # Manufacturer Code
        RList.append(row[1]+row[2])                     # Manufacturer Part Number
        RList.append(fixSQLdate(row[4]))                # Date Stamp
        RList.append(row[-2])                           # Package Type
        RList.append(row[5])                            # ATS

        if row[7] == None: continue                     # Skip if NO price break info available

        if row[6] == None:                              # Minimum Package Size
            RList.append(row[7])                        # Use first price break if NULL
        else:
            RList.append(row[6])                        # Use provided value

#       Add price breaks (quantity,price) to list
        i = 7
        while (i < len(row)-2):                         # Scan price tiers (Quantity,Price)
            if RList[0]=="TTI":
                if row[i]==None or row[i]==0: break     # Break out if quantity=NONE found for TTI values
                if row[i + 1] == None:
                    break                               # Break if price is NONE
                else:
                    rval = float(row[i + 1])
                tupval = (row[i], rval)
                RList.append(tupval)
            else:                                       # Create (Q,P) tuple
                if row[i] == None: break
                tupval = (row[i], float(row[i + 1]))
                RList.append(tupval)
            i += 2
        ix += 1
        PrDict[ix] = RList                              # Store in PrDict
    return PrDict                                       # Return Dictionary -- PrDict

# Date Fix
def fixSQLdate(dateval):
    nd = str(dateval).split(" ")            # Return only date
    return nd[0]

def SQL_GetAllocData(CONNX, MFR, TestPartNum ):
    try:
        Alloc_cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: PRICING_COMBINE
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PART_INPUTS_Scoring] \
                WHERE [Mfg] = ? And [mfrPartNumber] = ?  """

    Alloc_cursor.execute(SQLquery,(MFR, TestPartNum))                          # Fetch Allocation Data
    Allocation = 0
    for row in Alloc_cursor: Allocation = int(row[2])

    return Allocation

def SQL_GetMCData(CONNX, MFR, TestPartNum ):
    try:
        MC_cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: PRICING_COMBINE
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PART_INPUTS_Info] \
                WHERE [Mfg] = ? And [mfg Part Number] = ?  """

    MC_cursor.execute(SQLquery,(MFR, TestPartNum))                          # SQL Query Execution

    MC   = 'N'
    SOAI = ''
    for row in MC_cursor:
        MC   = row[15]
        SOAI = row[16]

    return (MC,SOAI)

# Make list of tuples (Q,P) for customer specific pricing
def SQL_CustSpecPricing(CONNX, TestPartNum, Manufacturer, AllTiers):
    CSPList = []
    try:
        CSPcursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: BUSINESS_RULES
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[BUSINESS_RULES] 
                WHERE [MfrPartNumber] = ? And [Mfg] = ? """

    CSPcursor.execute(SQLquery,(TestPartNum, Manufacturer))       # SQL Query Execution

    TDict = {}
    for row in CSPcursor:
        TDict[int(row[2])] = float(row[3])

    CSPList = []
    P = -99999.99
    for TierQ in AllTiers:
        if TierQ in TDict:
            newCSP = TDict[TierQ]
            if newCSP > P: P = newCSP
        NTup = (TierQ,P)
        CSPList.append(NTup)

    for ix,(Q,P) in enumerate(CSPList):
        if P < -9999.9: P = np.nan
        CSPList[ix] = (Q,P)

    CSPList.sort()
    return CSPList