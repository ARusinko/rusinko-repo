#---------------------------------------------------------------------------------------------------------------
#
#           ScoringFunctions v1.0: Functions used to manipulate data used in Scoring Functions.
#
#                                   A. Rusinko  (11/30/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
from math import exp
import numpy as np

# Calculate score based on TTI price differential vs mean or min of competitors
def CalcPriceScore(F1,F2,dfr):
    (nrows,ncols) = dfr.shape
    dfr['OriPriceScore'] = [0.0]*nrows     # Initialize InvenScore
    for index, row in dfr.iterrows():
        dfr.loc[index,'OriPriceScore'] = calcScore1(F1,F2,dfr.loc[index,'TTI_Price'],
                                        dfr.loc[index,'CompAve'],dfr.loc[index,'CompMin'])
    pass
#
# Simple price scoring function based on weighted differeneces between price
# and competitor mean and min price.
#
def calcScore1(F1,F2, x,ave,mval):
    if np.isnan(ave) or np.isnan(x) or x<0.0000001: return np.NaN
    XtoMin = (x - mval) /mval
    XtoAve = (x - ave) / ave
    Score1 = (F1*XtoMin + F2*XtoAve) / (F1+F2)
    return Score1

#  Re-establish constant score for each TTI price tier via a weighted average in tier
def MakeTierScores(dfr):
    (nrows,ncols) = dfr.shape
    WAve = []
    ilast = 0
    summa = 0.0
    quant = 0.0
    for ix in range(0,len(dfr)):
        if dfr.iloc[ix]['TTI_Index'] > ilast:
            summa = dfr.iloc[ix]['MinQ']*dfr.iloc[ix]['OriPriceScore']                  # Setup Next Price Tier
            quant = dfr.iloc[ix]['MinQ']
            ilast = dfr.iloc[ix]['TTI_Index']

            for jx in range(ix+1,len(dfr)):                                             # Add additional values in tier
                if dfr.iloc[jx]['TTI_Index'] > ilast: break
                if np.isnan(dfr.iloc[jx]['OriPriceScore']):
                    break
                else:
                    summa += dfr.iloc[jx]['MinQ']*dfr.iloc[jx]['OriPriceScore']
                    quant += dfr.iloc[jx]['MinQ']

        if np.isnan(dfr.iloc[ix]['OriPriceScore']):             # Compute and add weighted ave to list
            WAve.append(np.NaN)
#            print("\t", ix, dfr.iloc[ix]['TTI_Index'], ilast, summa, quant, np.NaN)
        elif   quant > 0.0000001:
            WAve.append(summa/quant)
#            print("\t",ix, dfr.iloc[ix]['TTI_Index'], ilast,summa,quant,summa/quant)
        else:
            WAve.append(np.NaN)
#            print("\t", ix, dfr.iloc[ix]['TTI_Index'], ilast, summa, quant, np.NaN)

    dfr['WgtPriceScore'] = WAve                                                         # Update dataframe

    pass

def GetQuantityBetweenBreaks(dfr):
    iprev = (dfr.iloc[0]['MinQ'])                       # Get first quantity break value
    QList = []
    for index, row in dfr.iterrows():
        xf = index - iprev
        if xf <1.0: xf =1.0
        QList.append(xf)
        iprev = index

    QList.append(dfr.iloc[-1]['MinQ'])
    QList.pop(0)
    dfr['QWeights'] = QList
    pass
#
#               Calculate Inventory Score per price break
#
def CalcInvenScore(dfr, PDict):
    (nrows,ncols) = dfr.shape
    dfr['InvenScore'] = [0.0]*nrows     # Initialize InvenScore

    ATSList = []
    StPList = []
    for dk in PDict:                    # Get Competitor ATS and StPack size
        row = PDict[dk]
        if 'TTI' in row[0] or 'MOU' in row[0]: continue
        ATSList.append(row[4])
        StPList.append(row[5])

    for index, row in dfr.iterrows():                          # index is quantity at price break
        if index<dfr.loc[index, 'StdPackage']: continue        # Ignore Tier if less than standard package
        if dfr.loc[index, 'TTI_ATS']<index:    continue        # or not enough TTI stock

        sum   = 0.0
        for ix in range(0,len(StPList)):
            if index<StPList[ix]: continue
            if ATSList[ix]<index: continue
            sum += ATSList[ix]

        dfr.loc[index,'InvenScore'] = dfr.loc[index,'TTI_ATS'] / (float(sum)+dfr.loc[index,'TTI_ATS'])
    pass
#
#                               Modify prices based on scores
#
def CalcNewPrice(dfr, L=6.0):
    (nrows, ncols) = dfr.shape
    L2   = 0.5*L

    print(dfr[['TTI_Price','WgtPriceScore','InvenScore']])

    XOut = []
    for index, row in dfr.iterrows():
        Update = CalcPrice(dfr.loc[index, 'TTI_Price'],dfr.loc[index, 'WgtPriceScore'],dfr.loc[index,'InvenScore'],L2)
        XOut.append(Update)
gas
    passn   Me

def CalcPrice(Price, WPS, InvS, L2):
    NewPrice = np.NaN
    if np.isnan(Price): return NewPrice                     # No price to update
    if WPS > 1.0: WPS = 1.0                                 # Max for weighted Price Score

    if InvS >0.75:
        Correction =  L2*(1.0 - 2.0/(1.0+exp(-10.0*WPS))) /100.0
    else:
        Correction =  L2*(1.0 - 2.0/(1.0+exp( -5.0*WPS))) /100.0

    NewPrice = (1.0 + Correction) * Price
    return NewPrice


