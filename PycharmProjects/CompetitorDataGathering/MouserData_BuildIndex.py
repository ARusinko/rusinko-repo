#---------------------------------------------------------------------------------------------------------------
#
#               MouserData_BuildIndex:  Generate a file with Mouser/TTI/Mfr Partnos.
#
#                                   A. Rusinko  (10/20/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import time
import pickle

__author__ = 'ARusinko'

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

    MouserDataIndex = {}
    icnt = 0
    f = open('./Mouser1026.txt','r')

    for line in f:
        print(line)
        icnt += 1

# Pickle the 'data' dictionary using the highest protocol available.
#    with open('mouserdata.pickle', 'wb') as f:
#        pickle.dump(MouserDataIndex,f, pickle.HIGHEST_PROTOCOL)

    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

