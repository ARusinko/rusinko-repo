#---------------------------------------------------------------------------------------------------------------
#
#           PricingData_BuildIndex: Read TTI pricing data and generate an index to each
#                                   record in flat file.
#
#                                   A. Rusinko  (10/20/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import time
import pickle

__author__ = 'ARusinko'

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

    PriceDataIndex = {}
    icnt = 0
    with open('./PricingCombine0.txt','r') as f:
        xpos = 0
        line = f.readline()
        zline = line.splitlines()
        xline = zline[0].split('\t')
#        print(xline[0], xpos)
        PriceDataIndex[xline[0]] = xpos
        icnt += 1

        while line:
            xpos = f.tell()                             # returns the location of the next line
            line = f.readline()
            if ("" == line): break                      # EOF break

            zline = line.splitlines()                   # Remove \n
            xline = zline[0].split('\t')                # Split line into list by tab seperator

            if xline[0]=="$$$$": continue               # Record terminator

            if xline[1]=='Cur' or xline[1]=='Obs':
                if icnt % 1000 ==0: print(xline[0],xpos)
                PriceDataIndex[xline[0]] = xpos
                icnt += 1

    # Pickle the 'data' dictionary using the highest protocol available.
    with open('data.pickle', 'wb') as f:
        pickle.dump(PriceDataIndex,f, pickle.HIGHEST_PROTOCOL)

    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

