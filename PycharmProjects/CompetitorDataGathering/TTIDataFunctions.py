import sys
import pyodbc
import numpy as np


XZERO  = 0.00000001


# SQL_GetCompPriceData By Manufacturer Part Number, reformat and return dictionary of relevant data
def SQL_GetCompPriceData(TestPartNum, minATS):

    #   Establish Connection to SQL_SERVER -- Competitor Web Prices Table
    CONNX = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=TTIDW;'
                           'Trusted_Connection=yes;')
    try:
        cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

    SQLquery = """SELECT [CompetitorWebPricesKey],dc.[CompetitorCode],  cp.[ManufacturerPartNumber]  \
               ,[DateReturnedKey],[AvailableToSell],[StandardPackage] \
               ,[QuantityBreak1],[PriceBreak1],[QuantityBreak2],[PriceBreak2],[QuantityBreak3],[PriceBreak3] \
               ,[QuantityBreak4],[PriceBreak4],[QuantityBreak5],[PriceBreak5],[QuantityBreak6],[PriceBreak6] \
               ,[QuantityBreak7],[PriceBreak7],[QuantityBreak8],[PriceBreak8],[QuantityBreak9],[PriceBreak9] \
               FROM [TTIDW].[dbo].[FactCompetitorWebPrices] wp \
               JOIN [TTIDW].[dbo].[DimCompetitor] dc ON dc.[CompetitorKey] = wp.[CompetitorKey] \
               JOIN [TTIDW].[dbo].[DimCompetitorProduct] cp ON cp.[CompetitorProductKey] = wp.[CompetitorProductKey]\
               WHERE cp.[ManufacturerPartNumber] = ? and [AvailabletoSell]>=?; """

    cursor.execute(SQLquery,(TestPartNum,minATS))          # SQL Query Execution

    PrDict = {}
    for row in cursor:
        DKey   = row[0]
        RList  = []                     # Create List for each record
        RList.append(row[1])            # Competitor
        RList.append(row[2])            # Manufacturer
        RList.append(str(row[3]))       # DateReturnedKey
        RList.append(row[4])            # ATS

        if row[5] == None:              # Minimum Package Size
            RList.append(row[6])        # Use first price break if NULL
        else:
            RList.append(row[5])        # Use provided value

        i = 6
        while (i<len(row)):             # Price break tuples (Quantity,Price)
            if row[i]==None: break
            tupval = (row[i],float(row[i+1]))
            i+=2
            RList.append(tupval)

        PrDict[DKey] = RList            # Store in PrDict

    CONNX.close()
    return PrDict                       # Return Dictionary -- PrDict

# Get combined list of all break points
def GetPriceBreakPoints(PDict):
    PriceBreakQuantity = []
    for p in PDict:
        RList = PDict[p]
        for (q,p) in RList[5:]:
            if q in PriceBreakQuantity:
                continue
            else:
                PriceBreakQuantity.append(q)
    PriceBreakQuantity.sort()
    return PriceBreakQuantity

# Remove Price Break tuples if they exceed ATS, Available To Sell
def RemoveHighPriceBreaks(PDict,ZeroOK=False):
    for dk in PDict:
        RList = PDict[dk]
        ATS = RList[3]
        NList = []

        for i in range(5,len(RList)):
            (Q,P) = RList[i]
            if Q > ATS and ZeroOK==False: NList.append((Q,P))

        for nn in NList:
            RList.remove(nn)
    return PDict                    # Return Dictionary -- PrDict

# Create a vector of column names, Supplier(StandardPackage)+Date
def GetColumnNames(PDict):
    CName = []
    for dk in PDict:
        RList = PDict[dk]
        CName.append(RList[0]+"("+str(RList[4])+")")
    return CName                    # Return list of column names

def BuildQPMatrix(PDict, PBQ):
    lenPBQ    = len(PBQ)                    # lenPBQ -- Length of Price Break Quantity Vector
    lenVal    = len(PDict)+2                # lenVal -- Length of Pricing Dictionary + Avg, Min

    # Create default Quantity/Price array of all NEGATIVE ones
    QPMat = -1.0 * np.ones((lenPBQ, lenVal))

    # Add Price Data
    for ix,dk in enumerate(PDict):
        RList = PDict[dk]
        for (Q,P) in RList[5:] :
            jy = PBQ.index(Q)
            QPMat[jy,ix] = P

    # Fill in price matrix by using lower break if available
    for j in range(lenVal - 2):
        xlast = -1.0
        for i in range(lenPBQ):
            if QPMat[i,j] > XZERO:
                xlast = QPMat[i,j]
            elif QPMat[i,j] < 0.0:
                QPMat[i, j] = xlast
    return QPMat                            # Return Quantity/Price Matrix

# Add Mean and Minimun columns to Quantity/Price Matrix
def AddMeanMin(QPMat):
    (nrows,ncols) = QPMat.shape
    for i in range(nrows):
        sum = 0.0
        nct = 0
        mvl = 9999999.99
        for j in range(ncols):
            val = QPMat[i,j]
            if val>0.00:
                sum += val
                nct += 1
                if val < mvl: mvl = val
            else:
                QPMat[i, j] = np.NaN

        if nct==0:
            QPMat[i, ncols - 2] = np.NaN
            QPMat[i, ncols - 1] = np.NaN
        else:
            average = sum / float(nct)
            QPMat[i,ncols-2] = average
            QPMat[i,ncols-1] = mvl

    return QPMat                            # Return Updated Quantity/Price Matrix

# Canonacolize Pricing Dictionary by Supplier and PackageSize
def CanonPriceDictionay(PDict,ZeroOK = False):
    MList = []
    for dk in PDict:
        RList = PDict[dk]
        ptup  = (RList[2],RList[0],RList[4],RList[3],dk)
        MList.append(ptup)
    MList.sort(reverse=True)

    NList = []
    for (a,b,c,d,e) in MList:
        if ZeroOK == False and d==0:
            del PDict[e]
            continue
        tup = (b,c)
        if tup in NList:
            del PDict[e]
        else:
            NList.append(tup)
    return PDict                            # Return Updated Pricing Dictionary
