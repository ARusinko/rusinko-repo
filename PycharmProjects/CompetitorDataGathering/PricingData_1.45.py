#---------------------------------------------------------------------------------------------------------------
#
#           PricingData: Gather TTI pricing data from SQL Server and construct a matrix of
#                        quantity/price break information. Ouput to SQL Server and add Customer
#                        Specific Pricing. Calculate InventoryShare by price tier. Include Allocation
#                        attribute as well.
#
#                                   A. Rusinko  (11/15/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import pyodbc
from math import exp
import numpy as np
import time
from bisect import *

__author__ = 'ARusinko'

DEBUGLevel = "N"                                            # Levels = 'N,D,Q,M'

SCodes = {
    'www.ttiinc.com': 'TTI', 'www.peigenesis.com': 'PEI', 'www.newark.com': 'NRK',
    'www.futureelectronics.com': 'FUT', 'www.digikey.com': 'DKY', 'www.arrow.com': 'ARW',
    'avnetexpress.avnet.com': 'AVN', 'estore.heilind.com': 'HEI', 'www.mouser.com': 'MOU'}

MINBREAK = 20000                                                                   # STOP after exceeding MINBREAK

#   Dictionary for test cases --- mpno and mfr
TTIDict = {'518148321':'TYC', 'TNPW060310K9BEEA':'DAL',
    '0830000083':'MOL', '0002061103':'MOL','0002092133':'MOL', 'SM20ML1TK6':'SOU',
            'RK73H1ETTP4641F':'KOA','14803190':'TYC', '14899511':'TYC',
            '009155004201006':'AVX',  'NPC1210015A1S':'AAS', '353536526':'TYC',
            '1.5SMC400A':'BOU', '00021900':'ACO', '0002066100':'MOL', '0011400123': 'MOL',
            '00111920202':'PCD','B3FS4052P':'OMR',
            'Y6408201V1534300':'DEU', 'ACJSMHDE':'AAL', 'UVR2CR47MED1TD':'NIC',
            '#A915AY100M=P3':'MUR','T495X226K025ATE225':'KEM', '000000000004100':'AIR'}

# SQL_GetCompPriceData By Manufacturer Part Number, reformat and return dictionary of relevant data
def SQL_GetPriceData(CONNX, TestPartNum, Manufacturer, minATS=0 ):
    global SCodes
    try:
        cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: PRICING_COMBINE
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] \
                WHERE [MfrPartNumberCleaned] = ? And [Mfg] = ? And [ATS] >= ? """

    cursor.execute(SQLquery,(TestPartNum, Manufacturer, minATS))       # SQL Query Execution

    PrDict = {}
    ix = 0
    for row in cursor:
        if "D" in DEBUGLevel: print(row)
        RList  = []                                     # Create List for each record
        RList.append(SCodes[row[0]])                    # Manufacturer Code
        RList.append(row[1]+row[2])                     # Manufacturer Part Number
        RList.append(fixSQLdate(row[4]))                # Date Stamp
        RList.append(row[-2])                           # Package Type
        RList.append(row[5])                            # ATS

        if row[7] == None: continue                     # Skip if NO price break info available

        if row[6] == None:                              # Minimum Package Size
            RList.append(row[7])                        # Use first price break if NULL
        else:
            RList.append(row[6])                        # Use provided value

#       Add price breaks (quantity,price) to list
        i = 7
        while (i < len(row)-2):                         # Scan price tiers (Quantity,Price)
            if RList[0]=="TTI":
                if row[i]==None or row[i]==0: break     # Break out if quantity=NONE found for TTI values
                if row[i + 1] == None:
                    break                               # Break if price is NONE
                else:
                    rval = float(row[i + 1])
                tupval = (row[i], rval)
                RList.append(tupval)
            else:                                       # Create (Q,P) tuple
                if row[i] == None: break
                tupval = (row[i], float(row[i + 1]))
                RList.append(tupval)
            i += 2
        ix += 1
        PrDict[ix] = RList                              # Store in PrDict
    return PrDict                                       # Return Dictionary -- PrDict

# Date Fix
def fixSQLdate(dateval):
    nd = str(dateval).split(" ")
    return nd[0]

def SQL_GetAllocData(CONNX, MFR, TestPartNum ):
    try:
        Alloc_cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: PRICING_COMBINE
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PART_INPUTS_BASIC] \
                WHERE [Mfg] = ? And [MfrPartNumber] = ?  """

    Alloc_cursor.execute(SQLquery,(MFR, TestPartNum))                          # SQL Query Execution

    Allocation = 0
    for row in Alloc_cursor:
       Allocation = int(row[2])

    return Allocation

# Make list of tuples (Q,P) for customer specific pricing
def SQL_CustSpecPricing(CONNX, TestPartNum, Manufacturer, AllTiers):
    CSPList = []
    try:
        CSPcursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server -- table: BUSINESS_RULES
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[BUSINESS_RULES] 
                WHERE [MfrPartNumber] = ? And [Mfg] = ? """

    CSPcursor.execute(SQLquery,(TestPartNum, Manufacturer))       # SQL Query Execution

    TDict = {}
    for row in CSPcursor:
        TDict[int(row[2])] = float(row[3])

    CSPList = []
    P = -99999.99
    for TierQ in AllTiers:
        if TierQ in TDict:
            newCSP = TDict[TierQ]
            if newCSP > P: P = newCSP
        NTup = (TierQ,P)
        CSPList.append(NTup)

    for ix,(Q,P) in enumerate(CSPList):
        if P < -9999.9: P = np.nan
        CSPList[ix] = (Q,P)

    CSPList.sort()
    return CSPList

# Get combined list of all break points
def GetPriceBreakPoints(PDict):
    PriceBreakQuantity = []
    for p in PDict:
        RList = PDict[p]
        for (q,p) in RList[6:]:
            if q in PriceBreakQuantity:
                continue
            else:
                PriceBreakQuantity.append(q)
    PriceBreakQuantity.sort()
    return PriceBreakQuantity

# Remove Price Break tuples if they exceed ATS, Available To Sell
def RemoveHighPriceBreaks(PDict,ATSOK=False):
    for dk in PDict:
        RList = PDict[dk]
        ATS = RList[4]
        DictRemove = []
        NList      = []
        for i in range(6,len(RList)):
            (Q,P) = RList[i]
            if Q > ATS and ATSOK==False: NList.append((Q,P))
        for nn in NList:
            RList.remove(nn)

        if len(RList)==6: DictRemove.append(dk)         # Remove entries with NO tiers left

    for dk in DictRemove:
        del PDict[dk]
    return PDict                            # Return Dictionary -- PrDict

# Create a vector of column names, Supplier(StandardPackage)
def GetColumnNames(PDict):
    CName    = []
    NumbComp = 0
    for dk in PDict:
        RList = PDict[dk]
        if RList[0] != "TTI": NumbComp += 1
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        CName.append(ColName)
    return (NumbComp,CName)                            # Return list of column names

# Create a dictionary (competitors,index)
def GetNameIndex(PDict):
    Idx = {}
    for dk in PDict:
        RList = PDict[dk]
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        Idx[ColName] = dk
    return Idx

# Generate a Quantity/Price Matix and return (QPMat)
def BuildQPMatrix(PDict, PBQ):
    lenPBQ    = len(PBQ)                    # lenPBQ -- Length of Price Break Quantity Vector
    lenVal    = len(PDict)                  # lenVal -- Length of Pricing Dictionary

    # Create default Quantity/Price array of all NEGATIVE ones
    QPMat = -1.0 * np.ones((lenPBQ, lenVal))

    ATSList = []
    MaxTier = []
    # Add Price Data
    for ix,dk in enumerate(PDict):
        RList = PDict[dk]
        ATSList.append(RList[4])            # Create of list of ATS by vendor

        if len(RList)>6:
            (Q,P) = RList[-1]                   # Max quantity break
        else:
            Q = 0
        MaxTier.append(Q)

        for (Q,P) in RList[6:]:             # Matrix TRANSPOSE
            jy = PBQ.index(Q)
            QPMat[jy,ix] = P

    # Fill in price matrix by using lower break if available
    for j in range(lenVal):
        ATS = ATSList[j]                    # ATS for check of exceeding price breaks
        MxQ = MaxTier[j]

        xlast = -1.0
        for i in range(lenPBQ):
            if (PBQ[i]>ATS):
                break                           # Greater than available to sell, break
            else:
                if (PBQ[i]>MxQ): break          # Break if bigger than MaxTier

            if QPMat[i,j] > 0.00000001:         # Store Price value
                xlast = QPMat[i,j]
            elif QPMat[i,j] < 0.0:
                QPMat[i, j] = xlast
    return QPMat                                # Return Quantity/Price Matrix

# Computed quantity data fields -- return XPMat and MinVendor, Date Lists
def AddStats(QPMat,PDict,CompNames,TTIVal="TTI(1500)"):

#   Create a list of indices where TTI is found in CompNames list
    xidx = CompNames.index(TTIVal)

#   Get current number of price breaks --> nrows
    (nrows,ncols) = QPMat.shape

#   Create quantity pricing statistics matrix
    XPMat =  np.zeros((nrows, 6))

#   Add TTI Column of data
    for i in range(0,nrows):
        if QPMat[i,xidx] < 0.000:
            XPMat[i,0] = np.NaN
        else:
            XPMat[i,0] = QPMat[i,xidx]                                  # Add TTI data if it exists

    VList = []                                                          # List of Min vendors per tier
    DList = []                                                          # List of last date MinVendor

    for i in range(0, nrows):
        MList = []                                                      #  Vector of competitor prices at current tier
        LDict = {}
        jv    = 0
        miniv = 99999999.9
        for j in range(0, ncols):
            if "TTI" in CompNames[j] or "MOU" in CompNames[j]: continue     # Skip if TTI or Mouser

            if QPMat[i, j] < 0.000:
                continue
            else:
                MList.append(QPMat[i,j])                                    # Add REAL postive price data to list
                LDict[jv] = (CompNames[j], getPriceDate(CompNames[j],PDict))
                jv += 1

        ncnt = len(MList)
        if ncnt==0:
            XPMat[i,1:5] = np.NaN                       # NO competitor data
        else:
            XPMat[i,1] = sum(MList) / float(ncnt)       # Average competitor price
            XPMat[i,2] = min(MList)                     # Min competitor price
            miniv      = min(MList)
            XPMat[i,3] = max(MList)                     # Max competitor price
            XPMat[i,4] = XPMat[i,3] - XPMat[i,2]        # Range competitor price

#       Get vendor associated with min price
        try:
            xIdx = MList.index(miniv)
        except ValueError:
            xIdx = -1

        VMin = ''
        DMin = ''
        if xIdx>=0:  (VMin,DMin) = LDict[xIdx]          # Append to VList and DList
        VList.append(VMin[0:3])
        DList.append(DMin)

#       Any Mouser Data Loaded for Part?
        MouserData = False
        for cn in CompNames:
            if "MOU" in cn: MouserData = True

        # Append Mouser data
        if MouserData == True:
            for j in range(0, ncols):
                if "MOU" in CompNames[j]:
                    if QPMat[i,j] < 0.00001:
                        XPMat[i,5] =  np.NaN
                    else:
                        XPMat[i,5] = QPMat[i,j]
        else:
            for j in range(0, ncols): XPMat[i,5] =  np.NaN              # NO Mouser Data

    return (XPMat, VList, DList)

def getIndex(val, vlist):
    idx = -1
    if len(vlist) > 0:
        for idx,v in enumerate(vlist):
            if abs(val-v) < 0.00001: break
    return idx

def getPriceDate(CName, PDict):
    gdate = ''
    for dk in PDict:
        pr = PDict[dk]
        if CName[0:3]==pr[0]: gdate = pr[2]
    return gdate

# Canonacolize Pricing Dictionary by Distributor and PackageSize
def CanonPriceDictionay(PDict,ZeroOK = False):
    MList = []
    for dk in PDict:
        RList = PDict[dk]
        ptup  = (RList[2],RList[0],RList[5],RList[4],dk)
        MList.append(ptup)
    MList.sort(reverse=True)

    NList = []
    for (a,b,c,d,e) in MList:
        if ZeroOK == False and d==0:
            del PDict[e]
            continue
        tup = (b,c)
        if tup in NList:
            del PDict[e]
        else:
            NList.append(tup)
    return PDict                            # Return Updated Pricing Dictionary

# Calculate Average and Maximum ATS for all vendors
def getAveCompATS(PDict, CIdx):
    sum    = 0
    nct    = 0
    maxval = -99999999
    for cname in CIdx:
        if "TTI" in cname or "Mou" in cname: continue       # Skip TTI or Mou
        RList = PDict[CIdx[cname]]
        sum  += RList[4]
        nct  += 1
        if RList[4]>maxval: maxval = RList[4]               # Max ATS

    if  maxval<0.001: maxval = 0
    if nct>0:
        average = float(sum)/float(nct)
    else:
        average = 0.0
    return (average,maxval)

# Find max ATS for any TTI if multiple TTI records are present
def getMaxTTIATS(PDict):
    maxval = -99999999999
    for dk in PDict:
        SupplierCode = PDict[dk][0]
        if SupplierCode == "TTI":
            if PDict[dk][4] > maxval: maxval=PDict[dk][4]
    return maxval

def getTTI_Tiers(PrDict):
    PList = []
    for pr in PrDict:
        if "TTI" in PrDict[pr]:
            for (q, p) in PrDict[pr][6:]:
                if q not in PList: PList.append(q)
    PList.sort()
    return PList

# Calculate score based on TTI price differential vs mean or min of competitors
def calcScore1(F1,F2, x,ave,mval):
    if np.isnan(ave) or np.isnan(x) or x<0.0000001: return np.NaN
    XtoMin = (x - mval) /mval
    XtoAve = (x - ave) / ave
    Score1 = (F1*XtoMin + F2*XtoAve) / (F1+F2)
    return Score1

# Calculate pctInventoryShare
def calcScore2(PrDict,TTI_ATS):
    Score2 = np.NaN
    sumTTI = 0.0
    sum    = 0.0
    for dk in PrDict:
        pr = PrDict[dk]
#        print(pr)
        if pr[0] == "MOU": continue
        if pr[0] == "TTI": sumTTI += float(pr[4])
        sum      += float(pr[4])
    if sum>0.0000001: Score2 = sumTTI / sum
#    print(TTI_ATS,sumTTI,sum, Score2)
    return Score2

# Get bin value
def find_gt(a, x):
    'Find leftmost value greater than x'
    i = bisect_right(a, x)
    if i != len(a):
        return i-1
    raise ValueError

# Compare with Mouser Price -- return smaller of the two
def CheckMouser(TTIP, MouserP):
    val = TTIP
    if np.isnan(MouserP): return val
    if MouserP<TTIP:  val = MouserP                                                 # Compare with Mouser
    return val

# Check for errors in pricing -- should ALWAYS be in decreasing order
def CheckPriceOrder(PList):
    if len(PList)<2: return PList
    NList = [np.NaN] * len(PList)
#    print(PList)
    for ix in range(len(PList)-1, 0, -1):
        jx = ix-1
#        print(ix, PList[jx], PList[ix])
        if np.isnan(PList[ix]):                             # Skip, if NaN
            continue

        elif PList[ix]-PList[jx] >0.000000001:              # Found price break incongruency
            NList[ix] = PList[ix]
            for j in range(jx-1, 0, -1):
                if PList[j] - PList[ix] > 0.000000001:
                    jend = j
#                    print('\t',jend, PList[jend], PList[ix])
                    break
            else:
                for jt in range(jx, -1, -1):
                    if np.isnan(PList[jt]): continue
                    NList[jt] = 1.02 * PList[ix]
                    PList[jt] = NList[jt]
#                    print("\t\t", jt, PList[jt], NList[jt])
                continue

            deltax = float(jend) - float(ix)
            deltay = PList[jend] - PList[ix]
            slope  = deltay / deltax
            intcpt = PList[ix] - slope * float(ix)

            for ii in range(jend+1,ix):
                nval = slope*float(ii) + intcpt
                PList[ii] = nval
                NList[ii] = nval
        else:
            NList[ix] = PList[ix]

        if np.isnan(NList[0]) and not np.isnan(PList[0]): NList[0] = PList[0]
    return NList

#  Modify prices based on scores and Mouser prices
def CalcNewPrice(XPMat, Sc2, Sc1):
    (nrows, ncols) = XPMat.shape
    XOut = []
    for i in range(0, nrows):
        if np.isnan(XPMat[i,0]):                                    # SKIP no entry
            XOut.append(XPMat[i,0])
            continue
        else:
            score1 = Sc1[i]
            if score1 > 1.0: score1 = 1.0

        if Sc2[i]>0.75:
            Correction =  3.0*(1.0 - 2.0/(1.0+exp(-5.0*score1)))  /100.0
        else:
            Correction =  3.0*(1.0 - 2.0/(1.0+exp(-25.0*score1))) /100.0

        UpdatedPrice = (1.0 + Correction) * XPMat[i, 0]             # New UPDATED TTI Price
        XOut.append(UpdatedPrice)
    return XOut

#   Make an ordered list of TTI break points for ALL Tiers
def GetTTI_Tiers(AllTiers, TTITiers):
    Tiers = []
    TLevel = 0
    for anyTier in AllTiers:
        if anyTier in TTITiers: TLevel += 1
        Tiers.append(TLevel)
    return Tiers

#  Re-establish tiers based on constant scores
def MakeTierScores(AllTiers, TTITiers, ScoreVec):
    TTILevels = GetTTI_Tiers(AllTiers,TTITiers)         # Get TTI break points
    TierSet = set(TTILevels)
    TList = list(TierSet)                               # Get unique set of tier break points
    DeltaTiers = CalcBetweenTiers(AllTiers)             # Get quantity between breaks
    NewScoreVec = [np.nan] * len(ScoreVec)

#   Create a weighted average of scores within a price tier to establish new price for that tier
    for ttilev in TList:
        if ttilev == 0: continue
        sumval   = 0.0
        sumquant = 0.0
        for ix, tlev in enumerate(TTILevels):
            if tlev == ttilev:
                if not np.isnan(ScoreVec[ix]):
                    sumval   += ScoreVec[ix] * DeltaTiers[ix]
                    sumquant += DeltaTiers[ix]
        wave = np.NaN
        if sumquant>0.0: wave = sumval / sumquant                                   # Compute weighted average at tier
        for ix, tlev in enumerate(TTILevels):
            if tlev == ttilev: NewScoreVec[ix] = wave                               # Update scores
    return NewScoreVec

# Calculate deltas between tiers
def CalcBetweenTiers(Tiers):
    xn  = len(Tiers)
    Deltas    = [np.nan] * xn
    Deltas[0]  = float(Tiers[0])
    for i in range(1,len(Tiers)):
        Deltas[i] = float(Tiers[i] - Tiers[i-1] - 1.0)
        if Deltas[i] < 1.0: Deltas[i] = 1.0
    return Deltas

def ConvertNan2Null(val):
    vval = None
    if not np.isnan(val): vval = val
    return vval

def CalcInvShareTier(ATS, StdPckg, PBQ, PDict):
    TierShare = [0.0]*len(PBQ)
    ATSList = []
    MnPList = []
    for dk in PDict:
        row = PDict[dk]
        if 'TTI' in row[0] or 'MOU' in row[0]: continue
        ATSList.append(row[4])
        MnPList.append(row[5])

    for iq,Tier in enumerate(PBQ):
        if Tier<StdPckg: continue                   # Ignore Tier if less than standard package
        if ATS<Tier:     continue

        sum   = 0.0
        for ix in range(0,len(MnPList)):
            if Tier<MnPList[ix]: continue
            if ATSList[ix]<Tier: continue
            sum += ATSList[ix]

        invshare = float(ATS) / (float(sum)+float(ATS))
#        print(Tier, StdPckg, ATS, sum, invshare )

        TierShare[iq] = invshare
    return TierShare

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

#   Output file
    try:
        f = open('./PriceComparisontest.csv','w')
    except PermissionError:
        print("Can't open output csv file..... quitting")
        sys.exit()

#   Write out header row for csv file.
    f.write("Index, TTI_Index,VendorCode,ManufPartNum,ATS,StdPackage,Quantity,TTI,PriceUpdate,CSPrice,Mouser," +
            "CompAve,CompMin,CompMax,CompRange,CompAveATS,"+"CompMaxATS,MinVendor,LastUpdate,Score1,Score2,Allocation\n")

#   Get complete list of partnos and mfg
    CONN1 = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=PricingAnalytics;'
                       'Trusted_Connection=yes;')
    PartsList = CONN1.cursor()
    SQLquery = """ SELECT DISTINCT [MfrPartNumberCleaned], [mfg], [MfrPartNumber]
            FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] """
    PartsList.execute(SQLquery)

    #   Make new connection to database to write data
    CONN2 = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')
    SQLcursor = CONN2.cursor()

    # delete table if needed
    try:
        SQLcursor.execute("""DROP TABLE dbo.PriceComparison;""")
    except pyodbc.ProgrammingError:
        pass

    # SQL Insert command
    sql_command = """insert into dbo.PriceComparison(
        BreakCounter,TTI_BreakPoint,Vendor,MFRPartNumber,
        TTI_ATS,StdPackage,MinQuantity,TTI_Price,
        PriceUpdate,CustSpecPrice,MouserPrice,CompAvePrice,
        CompMiPricen,CompMaxPrice,CompPriceRange,CompAveATS,
        CompMaxATS,MinPriceComp,LastPriceDate,PriceScore,
        InventoryScore,AllocationScore) 
        VALUES(?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?,?,?,?,
               ?,?)"""

    # Create NEW Price Comparison table
    sql = """
        CREATE TABLE dbo.PriceComparison (
        BreakCounter int,
        TTI_BreakPoint varchar(1),
        Vendor varchar(3),
        MFRPartNumber varchar(50),
        TTI_ATS int,
        StdPackage int,
        MinQuantity int,
        TTI_Price decimal(12,4),
        PriceUpdate decimal(12,4),
        CustSpecPrice decimal(12,4),
        MouserPrice decimal(12,4),
        CompAvePrice decimal(12,4),
        CompMiPricen decimal(12,4),
        CompMaxPrice decimal(12,4),
        CompPriceRange decimal(12,4),
        CompAveATS decimal(12,4),
        CompMaxATS int,
        MinPriceComp varchar(3),
        LastPriceDate date,
        PriceScore decimal(12,4),
        InventoryScore decimal(12,4),
        AllocationScore int ); """

    SQLcursor.execute(sql)
    CONN2.commit()

#   New connection needed to retrieve data
    CONNX = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')
    icnt = 0
    for row in PartsList:
#        if icnt==0: break
        mfrpartno = row[0]                          # Find part by Mfg Part Number and Mfg
        mfg       = row[1]
        mpno      = row[2]

#    for mfrpartno in TTIDict:                       # For testing purposes on specific cases
#        mfg  = TTIDict[mfrpartno]
#        mpno = mfrpartno
#        print("\n",mfrpartno, mpno, mfg)

        if "," in mpno: continue                    # CODE AROUND Comma in ManPartNo, SKIP for now

#       Record count
        icnt+=1
        if icnt > MINBREAK: break

        if icnt%1000==0:
            end = time.time()
            timed = '{:.1f}'.format(end - start)
            print(icnt, mfrpartno,timed)

#       Get a dictionary containing lists of data by MnfPartNo and minimum ATS
#       minATS > 0 exlcudes any record with no stock on hand.
        PriceDict = SQL_GetPriceData(CONNX, mfrpartno, mfg, 1)              # TTI ATS must be at least 1
        if len(PriceDict)==0: continue

        if "D" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Keep only the most current entries in Pricing Dictionary, by Distributor and PackageSize
        PriceDict = CanonPriceDictionay(PriceDict,True)
        if "D" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Get Allocation attribute for part, mpno
        Allocation = SQL_GetAllocData(CONNX, mfg, mpno)

#       Remove Price Breaks when insufficient quantity on hand.
#       ZeroOK -- if True, return ALL price breaks found
        PriceDict = RemoveHighPriceBreaks(PriceDict,False)
        if len(PriceDict)==0: continue

        if "Q" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Get unique, sorted list of price breaks
        PBQ = GetPriceBreakPoints(PriceDict)
        if len(PBQ) == 0: continue                      # Done, if no data found
        if "Q" in DEBUGLevel:  print(PBQ)               # Echo list

        CSPList =[]
        CSPList = SQL_CustSpecPricing(CONNX, mpno, mfg, PBQ)

#       Create Column Names
        (NumbVendors,CName)  = GetColumnNames(PriceDict)        # Make a vector of column names
        if NumbVendors==0: continue                             # No competitors, skip
        if "Q" in DEBUGLevel: print(NumbVendors, CName)

#       Make list of TTI specific price tiers
        PList = getTTI_Tiers(PriceDict)

#       Make a dictionary(colname) = idx
        TTIIdx = GetNameIndex(PriceDict)

#       Loop  over ALL TTI values (if more than one)
        for cnm in TTIIdx:
            if "TTI" not in cnm: continue                               # SKIP parts with NO TTI information

            SQLdata = []                                                # SQL data for export per TTI(x)

#           Build Price Break Matrix
            QPMat =  BuildQPMatrix(PriceDict, PBQ)
            if "M" in DEBUGLevel: print(QPMat)

#           Add Competitor Pricing Stats and Min Vendor,Dates
            (XPMat, VList, DList) = AddStats(QPMat,PriceDict,CName,cnm)
            if "M" in DEBUGLevel: print(XPMat)

#           Output to flat file
            xp = PriceDict[TTIIdx[cnm]]
            (nrows,ncols) = XPMat.shape
            ATS     = xp[4]
            StdPckg = xp[5]

            if str(ATS)==ATS or str(StdPckg)==StdPckg:                  # Error check for junk in numeric column
                        print(PBQ)
                        for dk in PriceDict: print(PriceDict[dk])       # Flag error
                        break                                           # Don't output

            (AveCATS,MaxCATS) = getAveCompATS(PriceDict,TTIIdx)         # Get Average and Max Competitor ATS

            Max_TTIATS        = getMaxTTIATS(PriceDict)                 # Get Max TTI ATS
            if Max_TTIATS<max(PBQ) and MaxCATS<max(PBQ):
                print("\n",max(PBQ),ATS, MaxCATS,PBQ)
                for dk in PriceDict: print(PriceDict[dk])               # Flag error
                break                                                   # Don't output

            ave = '{:.1f}'.format(AveCATS)                              # Format average competitor ATS value

            Score2List = CalcInvShareTier(ATS, StdPckg, PBQ, PriceDict)         # Precalcuate ALL Score2 values
#            print(Score2List)

            Scores1 = []                                                        # Precalcuate ALL Score1 values
            for i in range(0,nrows):
                if Allocation==0:
                    score1 = calcScore1(1.0,1.0, XPMat[i,0], XPMat[i,1], XPMat[i,2])
                else:
                    score1 = calcScore1(1.0,3.0, XPMat[i,0], XPMat[i,1], XPMat[i,2])
                Scores1.append(score1)
            TieredScores = MakeTierScores(PBQ, PList, Scores1)

#           Calculate and Update Prices Based on Scoring Functions
            NewPrices = CalcNewPrice(XPMat,Score2List, TieredScores)             # Calculate update prices
#
#           Build up string for output to CSV file row by row
#
            for i in range(0,nrows):
                if np.isnan(XPMat[i,0]):                                # TTI
                    val0 = ''
                else:
                    val0 = '{:.4f}'.format(XPMat[i,0])

                if np.isnan(XPMat[i,5]) or XPMat[i,5]<0.000001:         # Mouser
                    Mouser = ''
                else:
                    Mouser = '{:.4f}'.format(XPMat[i,5])

                ixval = 'N'
                if PBQ[i] % StdPckg == 0: ixval = "P"
                if PBQ[i] in PList: ixval = "Y"                                         # TTI Tier Price Break

#               Format TTI/Mouser information from XPMat
                datastr0 = str(i+1)+","+ ixval +"," + \
                           mfg+ "," + str(mpno) + "," +str(ATS)+","+str(StdPckg)+ \
                                    ","+str(PBQ[i])+","+val0

                if np.isnan(NewPrices[i]) or NewPrices[i]<0.000001:                      # Update Prices
                    newPrice1 = ''
                else:
                    newPrice1 = '{:.4f}'.format(NewPrices[i])

                CSPrice  = ''
                CSPbreak = np.nan
                for (Q,P) in CSPList:
                    if Q == PBQ[i]:
                        CSPrice = ''
                        if not np.isnan(P): CSPrice = '{:.4f}'.format(P)
                        CSPbreak = P

                datastr0 = datastr0 + "," + newPrice1 + ","+ CSPrice  + "," + Mouser

#               Format competitor information from XPMat
                for j in range(1,ncols-1):
                    if np.isnan(XPMat[i, j]):
                        val = ''
                    else:
                        val = '{:.4f}'.format(XPMat[i, j])
                    datastr0 += "," + val

                if np.isnan(TieredScores[i]):                                           # Price Score
                    ScoreUpdate = ''
                else:
                    ScoreUpdate = '{:.4f}'.format(TieredScores[i])

                Score2Update = '{:.4f}'.format(Score2List[i])

                datastr0 = datastr0 +","+ave+","+str(MaxCATS)+","+VList[i]+","+DList[i] \
                           + "," + ScoreUpdate+","+Score2Update+","+str(Allocation)

#               Write to CSV file
                f.write('{0:s}\n'.format(datastr0))

#               Set up SQL write
                printvec = []
                for j in range(0, 6):
                    if np.isnan(XPMat[i, j]):
                        printvec.append(None)
                    else:
                        printvec.append(XPMat[i, j])

                NPi = ConvertNan2Null(NewPrices[i])
                Cbr = ConvertNan2Null(CSPbreak)
                SC1 = ConvertNan2Null(TieredScores[i])
                AvC = ConvertNan2Null(AveCATS)
                MxC = ConvertNan2Null(MaxCATS)

                VLi = VList[i]
                if len(VList[i]) == 0: VLi = None
                DLi = DList[i]
                if len(DList[i]) == 0: DLi = None

                score2 = Score2List[i]

                newtup = (i + 1, ixval, mfg, mpno, ATS,
                          StdPckg, PBQ[i], printvec[0], NPi, Cbr,
                          printvec[5], printvec[1], printvec[2], printvec[3], printvec[4],
                          AvC, MxC, VLi, DLi, SC1, score2, Allocation)
#                print(newtup)

                SQLdata.append(newtup)

            number_of_rows = SQLcursor.executemany(sql_command, SQLdata)
            CONN2.commit()

#   CLOSE files and connections
    f.close()
    CONN1.close()
    CONNX.close()
    CONN2.close()

#   Echo processing time
    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt-1,"Processed ---- Elasped:",timed,'s')
    pass


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)


