#---------------------------------------------------------------------------------------------------------------
#
#           PricingData: Gather TTI pricing data from SQL Server and construct a matrix of
#                        quantity/price break information. Include Mouser data as a separate
#                        column in v1.2.
#
#                                   A. Rusinko  (11/3/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import pyodbc
import numpy as np
import time

__author__ = 'ARusinko'

DEBUGLevel = "DQM"                                            # Levels = 'N,D,Q,M'

MINBREAK = 2

SCodes = {
    'www.ttiinc.com': 'TTI', 'www.peigenesis.com': 'PEI', 'www.newark.com': 'NRK',
    'www.futureelectronics.com': 'FUT', 'www.digikey.com': 'DKY', 'www.arrow.com': 'ARW',
    'avnetexpress.avnet.com': 'AVN', 'estore.heilind.com': 'HEI', 'www.mouser.com': 'MOU'}

# SQL_GetCompPriceData By Manufacturer Part Number, reformat and return dictionary of relevant data
def SQL_GetPriceData(CONNX, TestPartNum, Manufacturer, minATS=0 ):
    global SCodes
    try:
        cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

#   Fetch data for MfrPartNumber from SQL Server
    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] \
                WHERE [MfrPartNumberCleaned] = ? And [Mfg] = ? And [ATS] >= ? """

    cursor.execute(SQLquery,(TestPartNum, Manufacturer, minATS))       # SQL Query Execution

    PrDict = {}
    ix = 0
    for row in cursor:
        if "D" in DEBUGLevel: print(row)
        RList  = []                                     # Create List for each record
        RList.append(SCodes[row[0]])                    # Manufacturer Code
        RList.append(row[1]+row[2])                     # Manufacturer Part Number
        RList.append(fixSQLdate(row[4]))                # Date Stamp
        RList.append(row[-2])                           # Package Type
        RList.append(row[5])                            # ATS

        if row[7] == None: continue                     # Skip if NO price break info available

        if row[6] == None:                              # Minimum Package Size
            RList.append(row[7])                        # Use first price break if NULL
        else:
            RList.append(row[6])                        # Use provided value

#       Add price breaks (quantity,price) to list
        i = 7
        while (i < len(row)-2):                         # Scan price tiers (Quantity,Price)
            if RList[0]=="TTI":
                if row[i]==None or row[i]==0: break     # Break out if quantity=0 found for TTI values
                if row[i + 1] == None:
                    rval = 0.00
                else:
                    rval = float(row[i + 1])
                tupval = (row[i], rval)
                RList.append(tupval)
            else:                                       # Create (Q,P) tuple
                if row[i] == None: break
                tupval = (row[i], float(row[i + 1]))
                RList.append(tupval)
            i += 2

        ix += 1
        PrDict[ix] = RList                              # Store in PrDict
    return PrDict                                       # Return Dictionary -- PrDict

# Date Fix
def fixSQLdate(dateval):
    nd = str(dateval).split(" ")
    return nd[0]

# Get combined list of all break points
def GetPriceBreakPoints(PDict):
    PriceBreakQuantity = []
    for p in PDict:
        RList = PDict[p]
        for (q,p) in RList[6:]:
            if q in PriceBreakQuantity:
                continue
            else:
                PriceBreakQuantity.append(q)
    PriceBreakQuantity.sort()
    return PriceBreakQuantity

# Remove Price Break tuples if they exceed ATS, Available To Sell
def RemoveHighPriceBreaks(PDict,ATSOK=False):
    for dk in PDict:
        RList = PDict[dk]
        ATS = RList[4]

        NList = []
        for i in range(6,len(RList)):
            (Q,P) = RList[i]
            if Q > ATS and ATSOK==False: NList.append((Q,P))
        for nn in NList:
            RList.remove(nn)
    return PDict                            # Return Dictionary -- PrDict

# Create a vector of column names, Supplier(StandardPackage)
def GetColumnNames(PDict):
    CName    = []
    NumbComp = 0
    for dk in PDict:
        RList = PDict[dk]
        if RList[0] != "TTI": NumbComp += 1
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        CName.append(ColName)
    return (NumbComp,CName)                            # Return list of column names

# Create a dictionary (competitors,index)
def GetNameIndex(PDict):
    Idx = {}
    for dk in PDict:
        RList = PDict[dk]
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        Idx[ColName] = dk
    return Idx

# Generate a Quantity/Price Matix and return (QPMat)
def BuildQPMatrix(PDict, PBQ):
    lenPBQ    = len(PBQ)                    # lenPBQ -- Length of Price Break Quantity Vector
    lenVal    = len(PDict)                  # lenVal -- Length of Pricing Dictionary

    # Create default Quantity/Price array of all NEGATIVE ones
    QPMat = -1.0 * np.ones((lenPBQ, lenVal))

    ATSList = []
    # Add Price Data
    for ix,dk in enumerate(PDict):
        RList = PDict[dk]
        ATSList.append(RList[4])            # Create of list of ATS by vendor

        for (Q,P) in RList[6:]:             # Matrix TRANSPOSE
            jy = PBQ.index(Q)
            QPMat[jy,ix] = P

    # Fill in price matrix by using lower break if available
    for j in range(lenVal):
        ATS = ATSList[j]                    # ATS for check of exceeding price breaks

        xlast = -1.0
        for i in range(lenPBQ):

            if (PBQ[i]>ATS): break
#            print(j,i,ATS, PBQ[i])

            if QPMat[i,j] > 0.00000001:     # Store Price value
                xlast = QPMat[i,j]
            elif QPMat[i,j] < 0.0:
                QPMat[i, j] = xlast
    return QPMat                            # Return Quantity/Price Matrix

# Computed quantity data fields -- return XPMat and MinVendor, Date Lists
def AddStats(QPMat,PDict,CompNames,TTIVal="TTI(1500)"):

#   Create a list of indices where TTI is found in CompNames list
    xidx = CompNames.index(TTIVal)

#   Get current number of price breaks --> nrows
    (nrows,ncols) = QPMat.shape

#   Create quantity pricing statistics matrix
    XPMat =  np.zeros((nrows, 6))

#   Add TTI Column of data
    for i in range(0,nrows):
        if QPMat[i,xidx] < 0.000:
            XPMat[i,0] = np.NaN
        else:
            XPMat[i,0] = QPMat[i,xidx]                                  # Add TTI data if it exists

    VList = []                                                          # List of Min vendors per tier
    DList = []                                                          # List of last date MinVendor

    for i in range(0, nrows):
        MList = []                                                      #  Vector of competitor prices at current tier
        LDict = {}
        jv    = 0
        miniv = 99999999.9
        for j in range(0, ncols):
            if "TTI" in CompNames[j] or "MOU" in CompNames[j]: continue     # Skip if TTI or Mouser
            if QPMat[i, j] < 0.000:
                continue
            else:
                MList.append(QPMat[i,j])                # Add REAL postive price data to list
                LDict[jv] = (CompNames[j], getPriceDate(CompNames[j],PDict))
                jv += 1

        ncnt = len(MList)
        if ncnt==0:
            XPMat[i,1:5] = np.NaN                       # NO competitor data
        else:
            XPMat[i,1] = sum(MList) / float(ncnt)       # Average competitor price
            XPMat[i,2] = min(MList)                     # Min competitor price
            miniv      = min(MList)
            XPMat[i,3] = max(MList)                     # Max competitor price
            XPMat[i,4] = XPMat[i,3] - XPMat[i,2]        # Range competitor price

#       Get vendor associated with min price
        try:
            xIdx = MList.index(miniv)
        except ValueError:
            xIdx = -1

        VMin = ''
        DMin = ''
        if xIdx>=0:  (VMin,DMin) = LDict[xIdx]          # Append to VList and DList
        VList.append(VMin[0:3])
        DList.append(DMin)

#       Append Mouser data
        for j in range(0, ncols):
            if "MOU" in CompNames[j]:
                if QPMat[i,j] < 0.00001:
                    XPMat[i,5] =  np.NaN
                else:
                    XPMat[i,5] = QPMat[i,j]

    return (XPMat, VList, DList)

def getIndex(val, vlist):
    idx = -1
    if len(vlist) > 0:
        for idx,v in enumerate(vlist):
            if abs(val-v) < 0.00001: break
    return idx


def getPriceDate(CName, PDict):
    gdate = ''
    for dk in PDict:
        pr = PDict[dk]
        if CName[0:3]==pr[0]: gdate = pr[2]
    return gdate

# Canonacolize Pricing Dictionary by Distributor and PackageSize
def CanonPriceDictionay(PDict,ZeroOK = False):
    MList = []
    for dk in PDict:
        RList = PDict[dk]
        ptup  = (RList[2],RList[0],RList[5],RList[4],dk)
        MList.append(ptup)
    MList.sort(reverse=True)

    NList = []
    for (a,b,c,d,e) in MList:
        if ZeroOK == False and d==0:
            del PDict[e]
            continue
        tup = (b,c)
        if tup in NList:
            del PDict[e]
        else:
            NList.append(tup)
    return PDict                            # Return Updated Pricing Dictionary

# Calculate Average and Maximum ATS for all vendors
def getAveCompATS(PDict, CIdx):
    sum    = 0
    nct    = 0
    maxval = -99999999
    for cname in CIdx:
        if "TTI" in cname or "Mou" in cname: continue       # Skip TTI or Mou
        RList = PDict[CIdx[cname]]
        sum  += RList[4]
        nct  += 1
        if RList[4]>maxval: maxval = RList[4]               # Max ATS

    if  maxval<0.001: maxval = 0
    if nct>0:
        average = float(sum)/float(nct)
    else:
        average = 0.0
    return (average,maxval)

# Find max ATS for any TTI if multiple TTI records are present
def getMaxTTIATS(PDict):
    maxval = -99999999999
    for dk in PDict:
        SupplierCode = PDict[dk][0]
        if SupplierCode == "TTI":
            if PDict[dk][4] > maxval: maxval=PDict[dk][4]
    return maxval

def getTTI_Tiers(PrDict):
    PList = []
    for pr in PrDict:
        if "TTI" in PrDict[pr]:
            for (q, p) in PrDict[pr][6:]:
                if q not in PList: PList.append(q)
    PList.sort()
    return PList

# Calculate score based on TTI price differential vs mean or min of competitors
def calcScore1(F1,F2, x,ave,mval):
    if np.isnan(ave) or np.isnan(x) or x<0.0000001: return np.NaN
    XtoMin = (x - mval) /mval
    XtoAve = (x - ave) / ave
    Score1 = (F1*XtoMin + F2*XtoAve) / (F1+F2)
    return Score1

# Calculate pctInventoryShare
def calcScore2(PrDict,TTI_ATS):
    Score2 = np.NaN
    sumTTI = 0.0
    sum    = 0.0
    for dk in PrDict:
        pr = PrDict[dk]
#        print(pr)
        if pr[0] == "MOU": continue
        if pr[0] == "TTI": sumTTI += float(pr[4])
        sum      += float(pr[4])
    if sum>0.0000001: Score2 = sumTTI / sum
#    print(TTI_ATS,sumTTI,sum, Score2)
    return Score2

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

#   Dictionary for test cases --- mpno and mfr
    TTIDict = {'353536526':'TYC','B3FS4052P':'OMR',
                '0002066100':'MOL','0011400123': 'MOL',
                'Y6408201V1534300':'DEU', 'NPC1210015A1S':'AAS', 'ACJSMHDE':'AAL', 'UVR2CR47MED1TD':'NIC',
                '00111920202':'PCD', '001119-202-02':'PCD','#A915AY-100M=P3':'MUR',
               '1.5SMC400A':'BOU','SM20ML1TK6':'SOU','T495X226K025ATE225':'KEM',
               '00-000-000-000-4100':'AIR'}


    with open('data.pickle', 'rb') as f:
        Ranges = pickle.load(f)

    try:
 #       f = open('C:/Users/arusinko/Documents/TTI Stuff/PriceComparison.csv','w')
        f = open('./PriceComparisontest.csv','w')
    except PermissionError:
        print("Can't open output csv file..... quitting")
        sys.exit()

#   Write out header row for csv file.
    f.write("Index, TTI_Index,VendorCode,ManufPartNum,ATS,StdPackage,Quantity,TTI,Mouser,CompAve,CompMin,CompMax,"+
            "CompRange,CompAveATS,"+"CompMaxATS,MinVendor,LastUpdate,Score1a,Score2\n")

#   Get complete list of partnos and mfg
    CONN1 = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=PricingAnalytics;'
                       'Trusted_Connection=yes;')
    PartsList = CONN1.cursor()

#    SQLquery = """ SELECT Top 10000 [MfrPartNumberCleaned], [mfg]

    SQLquery = """ SELECT DISTINCT [MfrPartNumberCleaned], [mfg], [MfrPartNumber]
            FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] """
    PartsList.execute(SQLquery)

#   New connection needed to retrieve data
    CONNX = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')

    icnt = 0
#    for row in PartsList:
#        mfrpartno = row[0]                          # Find part by Mfg Part Number and Mfg
#        mfg       = row[1]
#        mpno      = row[2]

    for mfrpartno in TTIDict:                       # For testing purposes
        mfg  = TTIDict[mfrpartno]
        mpno = mfrpartno

#        print(mfrpartno, mpno, mfg)
        if "," in mpno: continue                    # CODE AROUND Comma in ManPartNo, SKIP for now

#       Record count
        icnt+=1
        if icnt > MINBREAK: break

        if icnt%500==0:
            end = time.time()
            timed = '{:.1f}'.format(end - start)
            print(icnt, mfrpartno,timed)

#       Get a dictionary containing lists of data by MnfPartNo and minimum ATS
#       minATS > 0 exlcudes any record with no stock on hand.
        PriceDict = SQL_GetPriceData(CONNX, mfrpartno, mfg, 0)

        if len(PriceDict)==0: continue

        if "D" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Keep only the most current entries in Pricing Dictionary, by Distributor and PackageSize
        PriceDict = CanonPriceDictionay(PriceDict,True)
        if "D" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Remove Price Breaks when insufficient quantity on hand.
#       ZeroOK -- if True, return ALL price breaks found
        PriceDict = RemoveHighPriceBreaks(PriceDict,False)
        if "Q" in DEBUGLevel:
            for pr in PriceDict: print (pr,PriceDict[pr])

#       Get unique, sorted list of price breaks
        PBQ = GetPriceBreakPoints(PriceDict)
        if len(PBQ) == 0: continue                      # Done, if no data found
        if "Q" in DEBUGLevel:  print(PBQ)               # Echo list

#       Create Column Names
        (NumbVendors,CName)  = GetColumnNames(PriceDict)        # Make a vector of column names
        if NumbVendors==0: continue                             # No competitors, skip
        if "Q" in DEBUGLevel: print(NumbVendors, CName)

#       Make list of TTI specific price tiers
        PList = getTTI_Tiers(PriceDict)

#       Make a dictionary(colname) = idx
        TTIIdx = GetNameIndex(PriceDict)

#       Loop  over ALL TTI values (if more than one)
        for cnm in TTIIdx:
            if "TTI" not in cnm: continue                               # SKIP parts with NO TTI information

#           Build Price Break Matrix
            QPMat =  BuildQPMatrix(PriceDict, PBQ)
            if "M" in DEBUGLevel: print(QPMat)

#           Add Competitor Pricing Stats and Min Vendor,Dates
            (XPMat, VList, DList) = AddStats(QPMat,PriceDict,CName,cnm)
            if "M" in DEBUGLevel: print(XPMat)

#           Output to flat file
            xp = PriceDict[TTIIdx[cnm]]
            (nrows,ncols) = XPMat.shape
            ATS     = xp[4]
            StdPckg = xp[5]

            if str(ATS)==ATS or str(StdPckg)==StdPckg:                  # Error check for junk in numeric column
                        print(PBQ)
                        for dk in PriceDict: print(PriceDict[dk])       # Flag error
                        break                                           # Don't output

            (AveCATS,MaxCATS) = getAveCompATS(PriceDict,TTIIdx)         # Get Average and Max Competitor ATS
            Max_TTIATS        = getMaxTTIATS(PriceDict)                 # Get Max TTI ATS

            if Max_TTIATS<max(PBQ) and MaxCATS<max(PBQ):
                print("\n",max(PBQ),ATS, MaxCATS,PBQ)
                for dk in PriceDict: print(PriceDict[dk])               # Flag error
                break                                                   # Don't output

            ave = '{:.1f}'.format(AveCATS)                              # Format average competitor ATS value

            score2 = calcScore2(PriceDict,ATS)                          # Test Score2
            if np.isnan(score2):
                score2 = ''
            else:
                score2 = '{:.2f}'.format(score2)

#           Build up string for output to CSV file
            for i in range(0,nrows):
                if np.isnan(XPMat[i,0]):                                # TTI
                    val0 = ''
                else:
                    val0 = '{:.4f}'.format(XPMat[i,0])

                if np.isnan(XPMat[i,5]) or XPMat[i,5]<0.00001:          # Mouser
                    val1 = ''
                else:
                    val1 = '{:.4f}'.format(XPMat[i,5])

                ixval = ''
                if PBQ[i] in PList: ixval = "*"             # TTI Tier Price Break

#               Format TTI/Mouser information from XPMat
                datastr0 = str(i+1)+","+ ixval +"," + \
                           mfg+ "," + str(mpno) + "," +str(ATS)+","+str(StdPckg)+ \
                                    ","+str(PBQ[i])+","+val0+","+val1

#               Format competitor information from XPMat
                for j in range(1,ncols-1):
                    if np.isnan(XPMat[i, j]):
                        val = ''
                    else:
                        val = '{:.4f}'.format(XPMat[i, j])
                    datastr0 += "," + val

                score1a = calcScore1(3.0,1.0, XPMat[i,0], XPMat[i,1], XPMat[i,2])         # Test Score1
                if np.isnan(score1a): score1a =''

#                score1b = calcScore1(2.0,2.0, XPMat[i,0], XPMat[i,1], XPMat[i,2])         # Test Score1
#                if np.isnan(score1b): score1b =''

#                score1c = calcScore1(1.0,3.0, XPMat[i,0], XPMat[i,1], XPMat[i,2])         # Test Score1
#                if np.isnan(score1c): score1c =''

#                datastr0 = datastr0 + ","+ave+","+str(MaxCATS)+","+VList[i]+","+DList[i]+","+str(score1a)+ \
#                           ","+str(score1b)+","+str(score1c)+","+score2
                datastr0 = datastr0 + ","+ave+","+str(MaxCATS)+","+VList[i]+","+DList[i]+","+str(score1a)+ ","+score2

#               Write to CSV file
                f.write('{0:s}\n'.format(datastr0))

#   Close files and connections
    f.close()
    CONN1.close()
    CONNX.close()

#   Echo processing time
    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)
