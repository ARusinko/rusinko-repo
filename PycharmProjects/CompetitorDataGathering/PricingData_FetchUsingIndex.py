#---------------------------------------------------------------------------------------------------------------
#
#           PricingData_FetchUsingIndex: Fetch part pricing data via an index to each record in flat file.
#
#                                   A. Rusinko  (10/20/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import time
import pickle

__author__ = 'ARusinko'

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()
    icnt = 0

    dfile = open('./PricingCombine0.txt','r')

    with open('data.pickle', 'rb') as f:
        PriceDataIndex = pickle.load(f)

    for dk in PriceDataIndex:
        icnt += 1
        if icnt%10000==0: print(dk, PriceDataIndex[dk])
        dfile.seek(PriceDataIndex[dk])
        line  = dfile.readline()
        zline = line.splitlines()
        xline = zline[0].split('\t')
#        print(xline,"\n")

    dfile.close()
    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)
