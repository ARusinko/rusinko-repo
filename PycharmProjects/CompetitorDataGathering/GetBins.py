#---------------------------------------------------------------------------------------------------------------
#
#                   GetBins:    Make bins based on equi-frequency scores.
#
#                                   A. Rusinko  (11/08/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import pickle
from bisect import *
from copy   import deepcopy
import numpy as np
from scipy.optimize import curve_fit

__author__ = 'ARusinko'

BINFile = './BinsHi.csv'
#DATFile = './ScoreTestHi.csv'
CSVFile = './TestScoresOut.csv'


def func(x, a, b):
    return a*np.log(x) + b

def BuildModel(xdat,ydat):
    xlen = len(xdat)
    xd = np.zeros(xlen)
    yd = np.zeros(xlen)
    for ix,xv in enumerate(xdat):
        xd[ix] = xdat[ix]
        yd[ix] = ydat[ix]
    popt, pcov = curve_fit(func, xd, yd)
    return popt

def CalcPred(xdat, popt):
    yp = []
    for x in xdat:
        yp.append(func(x,popt[0],popt[1]))
    return yp


# Get bin value
def find_gt(a, x):
    'Find leftmost value greater than x'
    i = bisect_right(a, x)
    if i != len(a):
        return i-1
    raise ValueError

def CheckList(XList):
    QList = deepcopy(XList)
    QList.sort(reverse=True)
    for ix,xval in enumerate(XList):
        if abs(xval-QList[ix]>0.00000001): raise ValueError
    pass

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):
    global INFile, DATFile

    HV = np.arange(3.0, -0.25, -0.25)/100.0
    HiVals = []
    for hx in HV: HiVals.append(hx)
    HiVals.append(-0.010)
    HiVals.append(-0.020)
    HiVals.append(-0.030)
#    print(HiVals)

#    o = open(CSVFile,'w')

    Ranges = {}
    BINFile = './BinsHi.csv'
    BinsDict = {}
    icnt = 0
    f = open(BINFile,'r')
#   Read in pricing scores to establish bin boundaries
    for line in f:
        icnt += 1
        if icnt==1: continue
        zline = line.splitlines()
        xline = zline[0].split(',')

        if xline[2] not in BinsDict:
            PVect = [float(xline[0])]
            BinsDict[xline[2]] = PVect
        else:
            BinsDict[xline[2]].append(float(xline[0]))
    f.close()

#   Find upper lower values of bin boundaries
    XList  = sorted(BinsDict, key=BinsDict.get)
    BinsHi  = {}
    for df in XList:
        PVect = BinsDict[df]
        PVect.sort()
        if df == XList[0]:
            HiBList = [-1.0]
        else:
            HiBList.append(float(PVect[-1]))
    HiBList.append(1000.0)
    print(len(HiBList),HiBList)
    Ranges['Hi'] = HiBList

    BINFile = './BinsLo.csv'
    BinsDict = {}
    icnt = 0
    f = open(BINFile, 'r')
    #   Read in pricing scores to establish bin boundaries
    for line in f:
        icnt += 1
        if icnt == 1: continue
        zline = line.splitlines()
        xline = zline[0].split(',')
        if xline[2] not in BinsDict:
            PVect = [float(xline[0])]
            BinsDict[xline[2]] = PVect
        else:
            BinsDict[xline[2]].append(float(xline[0]))
    f.close()

    #   Find upper lower values of bin boundaries
    XList = sorted(BinsDict, key=BinsDict.get)
    BinsHi = {}
    for df in XList:
        PVect = BinsDict[df]
        PVect.sort()
        if df == XList[0]:
            HiBList = [-1.0]
        else:
            HiBList.append(float(PVect[-1]))

    HiBList.append(1000.0)
    print(len(HiBList), HiBList)
    Ranges['Lo'] = HiBList

    # Pickle the 'data' dictionary using the highest protocol available.
    with open('ranges.pickle', 'wb') as p:
        pickle.dump(Ranges, p, pickle.HIGHEST_PROTOCOL)

    sys.exit()

    pass

# EXECUTE Main
if __name__ == "__main__":
    main(sys.argv)
