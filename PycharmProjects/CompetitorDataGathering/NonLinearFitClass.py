#---------------------------------------------------------------------------------------------------------------
#
#               NonLinearFit: Fit a non-linear function to smooth out pricing data by Class.
#
#                                   A. Rusinko  (11/01/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import numpy as np
from scipy.optimize import curve_fit
from scipy import stats

__author__ = 'ARusinko'

INFile =  './ClassThresholds11012017.txt'

def func(x, a, b):
    return a*np.log(x) + b

def BuildModel(xdat,ydat):
    xlen = len(xdat)
    xd = np.zeros(xlen)
    yd = np.zeros(xlen)
    for ix,xv in enumerate(xdat):
        xd[ix] = xdat[ix]
        yd[ix] = ydat[ix]
    popt, pcov = curve_fit(func, xd, yd)
    return popt

def CalcPred(xdat, popt):
    yp = []
    for x in xdat:
        yp.append(func(x,popt[0],popt[1]))
    return yp

def WriteBack(ofile, DF, yp, CAT, ival, R2):
    xlist = []
    for ix in DF:
        (xln, iv) = DF[ix]
        if iv==ival: xlist.append(ix)
    xlist.sort()

    for ii,ix in enumerate(xlist):
        (xln, iv) = DF[ix]
        if ii==0: xln[0] = CAT
        xtmp = ','.join(map(str,xln))
        ofile.write('%s,%.3f,%.3f\n' % (xtmp,yp[ii],R2))
    return

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):
    global INFile
    f       = open(INFile,'r')
    outfile = open("./testout.csv", 'w')

    icnt = 0
    ival = 0
    DFil = {}
    Category  = ''
    Class     = ''
    Commodity = ''

    for line in f:
        icnt += 1
        if icnt==1:
            zline = line.splitlines()
            xline = zline[0].split('\t')
            xline.append("Pred25%")
            xline.append("R^2")
            outfile.write(",".join(xline)+"\n")
            continue                                # Header Line

        zline = line.splitlines()
        xline = zline[0].split('\t')
        if float(xline[5])<1: xline[5]=1.00

        # New Class Model
        if xline[1] != Class:
            if icnt>2:
 #               print('\n',Category,Class,len(xdat))
                if len(xdat)>2:
                    popt  = BuildModel(xdat,ydat)
                    ypred = CalcPred(xdat, popt)
                    (RSq, b_s, r, tt, stderr) = stats.linregress(ydat,ypred)
#                    print('yp = %.3f*ln(x) + %.3f [r^2=%.3f]' % (popt[0],popt[1],RSq))
                    WriteBack(outfile, DFil, ypred, Category, ival, RSq)
                else:
                    print("skipping ",Commodity, Category, Class)

                ival+=1

            Category  = xline[0]
            Class     = xline[1]
            Commodity = xline[2]

            xdat = []
            ydat = []
            xdat.append(float(xline[5]))
            ydat.append(float(xline[10]))
            DFil[icnt] = (xline,ival)
            continue

        xdat.append(float(xline[5]))
        ydat.append(float(xline[10]))
        DFil[icnt] = (xline, ival)

    f.close()
    outfile.close()
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

