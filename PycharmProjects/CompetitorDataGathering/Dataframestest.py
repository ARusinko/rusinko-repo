import pyodbc
import cursor as cursor
import pandas as pd
import pandas.io.sql as psql
from pandas.core.reshape.reshape import unstack
from sqlalchemy import true, false

cnxn = pyodbc.connect ( 'Driver={SQL Server};'
                        'Server=TXSQLD08\TXSQLD08;'
                        'Port=1433'
                        'Database=Mouser;'
                        'Trusted_Connection=yes;' )

sql = """
SELECT [Domain]     
      ,[MfrPartNumber]
      ,QB1 as MinQty
      ,[Manufacturer]
      ,[MouserPartNumber]
      ,[VendorCode]
      ,[PB1]
      ,[ATS]
      ,[PriceDate]
  FROM [Mouser].[dbo].[CompetitorProducts]
where MfrPartNumber ='08055C472KAZ2A' and VendorCode = 'AVX'
Order by [Domain],QB1,[PriceDate]
"""

df = psql.read_sql(sql,cnxn)
print(df.dtypes) #datatypes
print(df)

numofobsrv = df.shape[0]
print(numofobsrv) # Number of Observations df.info() will also work , shape[1] - Number of columns in dataset

print(df.index) #How is the dataset indexed?

#Most Quantity

c = df.groupby('MfrPartNumber')
c = c.sum()
c = c.sort_values(['ATS'], ascending=False)
c.head(1)

#print(c)

d = df.groupby('Domain').count()
d = d.sort_values(['PriceDate'], ascending=False)
d.head(1)

#print(d)
"""

gh = df.groupby(['Domain','MfrPartNumber','VendorCode', 'MinQty'])
gh1 = gh.sort_values(['PriceDate'], ascending=False)
print(gh1)
#Turn int to float
dollarizer = lambda x: float(x[1:-1])
df.ATS = df.ATS.apply(dollarizer)
print(df.ATS)


#How many in a period
k=df.id.value_count().count()
print(k)
"""
cnxn.close ()

