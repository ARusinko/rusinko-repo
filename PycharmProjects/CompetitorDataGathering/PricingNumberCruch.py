#---------------------------------------------------------------------------------------------------------------
#
#           PricingDataTiming: Gather TTI pricing data from SQL Server and construct a matrix of
#                        quantity/price break information.
#
#                                   A. Rusinko  (10/20/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import time
import csv

__author__ = 'ARusinko'


#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

#   f = open('C:/Users/arusinko/Documents/TTI Stuff/PricingData1.csv', newline='')
    f = open('C:/Users/arusinko/Documents/TTI Stuff/Test1.csv','r')

    sys.exit()

    icnt = 0
    with open('C:/Users/arusinko/Documents/TTI Stuff/PricingData1.csv', newline='') as f:
        reader = csv.reader(f)

        for row in reader:
            icnt += 1
            if icnt > 1000: break
            print(row)


    f.close()

    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

