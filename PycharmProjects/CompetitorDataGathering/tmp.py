import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import kendalltau
import scipy.stats as stats
import sys

sns.set(style="ticks")


f2 = open("PriceScores.csv","r")
lines = f2.readlines()
f2.close()

x = []
y = []
ic = 0
fourcount = 0
for line in lines[1:]:
    p = line.split(",")
    xv = float(p[0])
    if xv>2.0:
        fourcount += 1
        continue
    x.append(float(p[0]))
    y.append(float(p[1]))

x_axis = np.asarray(x)
y_axis = np.asarray(y)

print(fourcount,len(lines))
sns.set(rc={'axes.facecolor':'cornflowerblue', 'figure.facecolor':'cornflowerblue'})
#sns.jointplot(x_axis, y_axis, kind="hex", stat_func=None, color="#4CB391")
#sns.jointplot(x_axis, y_axis, kind="hex", stat_func=None, color="red",size=8, ratio=3,).set_axis_labels("Pricing", "Quantity")
sns.jointplot(x_axis, y_axis, kind="hex", stat_func=None, color="blue").set_axis_labels("Pricing", "Inventory")

plt.show()