#---------------------------------------------------------------------------------------------------------------
#
#           PricingData: Gather TTI pricing data from an indexed flat file and construct a matrix of
#                        quantity/price break information. v1.1
#
#                                   A. Rusinko  (10/24/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import numpy as np
import pickle
import time
import pyodbc
import random

__author__ = 'ARusinko'

DEBUGLevel = "N"                            # Levels = 'N,D,Q,M'

# SQL_GetCompPriceData By Manufacturer Part Number, reformat and return dictionary of relevant data
def SQL_GetPriceData(CONNX, TestPartNum, Manufacturer, minATS=0 ):
    SCodes = {
    'www.ttiinc.com':'TTI','www.peigenesis.com':'PEI','www.newark.com':'NRK',
    'www.futureelectronics.com':'FUT','www.digikey.com':'DKY','www.arrow.com':'ARW',
    'avnetexpress.avnet.com':'AVN', 'estore.heilind.com':'HEI' }

    try:
        cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] \
                WHERE [MfrPartNumber] = ? And [Mfg] = ? And [ATS] >= ? """

    cursor.execute(SQLquery,(TestPartNum, Manufacturer, minATS))       # SQL Query Execution

    PrDict = {}
    ix = 0
    for row in cursor:
        if "D" in DEBUGLevel: print(row)
        RList  = []                                     # Create List for each record
        RList.append(SCodes[row[0]])                    # Manufacturer Code
        RList.append(row[1]+row[2])                     # Manufacturer Part Number
        RList.append(fixSQLdate(row[3]))                # Date Stamp
        RList.append(row[-1])                           # Package Type
        RList.append(row[4])                            # ATS

        if row[6] == None: continue                     # Skip if NO price break info available

        if row[5] == None:                              # Minimum Package Size
            RList.append(row[6])                        # Use first price break if NULL
        else:
            RList.append(row[5])                        # Use provided value

#       Add price breaks (quantity,price) to list
        i = 6
        while (i < len(row)-1):                         # Price break tuples (Quantity,Price)
            if RList[0]=="TTI":
                if row[i] == 0: break                   # Break out if quantity=0 found
                rval = float(row[i + 1])
                if rval < 0.00001: break                # Break out if price=0.0 found
                tupval = (row[i], rval)
                RList.append(tupval)
            else:
                if row[i] == None: break
                tupval = (row[i], float(row[i + 1]))
                RList.append(tupval)
            i += 2

        ix += 1
        PrDict[ix] = RList                              # Store in PrDict
    return PrDict                                       # Return Dictionary -- PrDict

# Date Fix
def fixSQLdate(dateval):
    nd = str(dateval).split(" ")
    return nd[0]


def FF_GetPriceData(csvf ,PDI, mpno, minATS=0 ):
    endcheck = ""
    ival     = 1
    PDict    = {}

    csvf.seek(PDI[mpno])
    line  = csvf.readline()
    zline = line.splitlines()
    xline = zline[0].split('\t')

    while endcheck != "$$$$":
        line = csvf.readline()
        zline = line.splitlines()  # Remove \n
        xline = zline[0].split('\t')  # Split line into list by tab seperator

        if len(xline)>1:
            if xline[3]=="None": xline[3] = None
            xline[4] = int(xline[4])
            xline[5] = int(xline[5])

            for ix,xx in enumerate(xline[6:]):
                xy = xx.replace("(","")
                xx = xy.replace(")","")
                xy = xx.split(",")
                xline[ix+6] = (int(xy[0]),float(xy[1]))
        endcheck = xline[0]
        if xline[0] != "$$$$": PDict[ival] = xline
        ival += 1
    return PDict

# Get combined list of all break points
def GetPriceBreakPoints(PDict):
    PriceBreakQuantity = []
    for p in PDict:
        RList = PDict[p]
        for (q,p) in RList[6:]:
            if q in PriceBreakQuantity:
                continue
            else:
                PriceBreakQuantity.append(q)
    PriceBreakQuantity.sort()
    return PriceBreakQuantity

# Remove Price Break tuples if they exceed ATS, Available To Sell
def RemoveHighPriceBreaks(PDict,ATSOK=False):
    for dk in PDict:
        RList = PDict[dk]
        ATS = RList[4]

        NList = []
        for i in range(6,len(RList)):
            (Q,P) = RList[i]
            if Q > ATS and ATSOK==False: NList.append((Q,P))

        for nn in NList:
            RList.remove(nn)
    return PDict                            # Return Dictionary -- PrDict

# Create a vector of column names, Supplier(StandardPackage)
def GetColumnNames(PDict):
    CName    = []
    NumbComp = 0
    for dk in PDict:
        RList = PDict[dk]
        if RList[0] != "TTI": NumbComp += 1
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        CName.append(ColName)
    return (NumbComp,CName)                            # Return list of column names

# Create a dictionary (competitors,index)
def GetNameIndex(PDict):
    Idx = {}
    for dk in PDict:
        RList = PDict[dk]
        ColName = RList[0] + "(" + str(RList[5]) + ")"
        Idx[ColName] = dk
    return Idx

# Generate a Quantity/Price Matix and return (QPMat)
def BuildQPMatrix(PDict, PBQ):
    lenPBQ    = len(PBQ)                    # lenPBQ -- Length of Price Break Quantity Vector
    lenVal    = len(PDict)                  # lenVal -- Length of Pricing Dictionary

    # Create default Quantity/Price array of all NEGATIVE ones
    QPMat = -1.0 * np.ones((lenPBQ, lenVal))

    ATSList = []
    # Add Price Data
    for ix,dk in enumerate(PDict):
        RList = PDict[dk]
        ATSList.append(RList[4])            # Create of list of ATS by vendor

        for (Q,P) in RList[6:]:
            jy = PBQ.index(Q)
            QPMat[jy,ix] = P

    # Fill in price matrix by using lower break if available
    for j in range(lenVal):
        ATS = ATSList[j]                    # ATS for check of exceeding price breaks

        xlast = -1.0
        for i in range(lenPBQ):

            if (PBQ[i]>ATS): break
#            print(j,i,ATS, PBQ[i])

            if QPMat[i,j] > 0.00000001:     # Store Price value
                xlast = QPMat[i,j]
            elif QPMat[i,j] < 0.0:
                QPMat[i, j] = xlast

    return QPMat                            # Return Quantity/Price Matrix

# Add computed quantity data fields
def AddStats(QPMat,PDict,CompNames,TTIVal="TTI(1500)"):

#   Create a list of indices where TTI is found in CompNames list
    xidx = CompNames.index(TTIVal)

#   Get current number of price breaks
    (nrows,ncols) = QPMat.shape

#   Create quantity pricing statistics matrix
    XPMat =  np.zeros((nrows, 5))

#   Add TTI Column of data
    for i in range(0,nrows):
        if QPMat[i,xidx] < 0.000:
            XPMat[i,0] = np.NaN
        else:
            XPMat[i,0] = QPMat[i,xidx]

    mincomp = []
    for i in range(0, nrows):
        miniv = 999999.0
        mname = ''
        MList = []
        for j in range(0, ncols):
            if "TTI" in CompNames[j]: continue
            if QPMat[i, j] < 0.000:
                continue
            else:
                MList.append(QPMat[i,j])
                if QPMat[i,j] < miniv:
                    miniv = QPMat[i,j]
                    mname = CompNames[j]

        mincomp.append(mname)                           # Store name of competitor with minimum value

        ncnt = len(MList)
        if ncnt==0:
            XPMat[i,1:5] = np.NaN
        else:
            XPMat[i,1] = sum(MList) / float(ncnt)
            XPMat[i,2] = min(MList)
            XPMat[i,3] = max(MList)
            XPMat[i,4] = XPMat[i,3] - XPMat[i,2]
    return (mincomp, XPMat)

# Canonacolize Pricing Dictionary by Supplier and PackageSize
def CanonPriceDictionay(PDict,ZeroOK = False):
    MList = []
    for dk in PDict:
        RList = PDict[dk]
        ptup  = (RList[2],RList[0],RList[5],RList[4],dk)
        MList.append(ptup)
    MList.sort(reverse=True)

    NList = []
    for (a,b,c,d,e) in MList:
        if ZeroOK == False and d==0:
            del PDict[e]
            continue
        tup = (b,c)
        if tup in NList:
            del PDict[e]
        else:
            NList.append(tup)
    return PDict                            # Return Updated Pricing Dictionary

# Calculate Average and Maximum ATS for all vendors
def getAveCompATS(PDict, CIdx):
    sum    = 0
    nct    = 0
    maxval = -99999999
    for cname in CIdx:
        if "TTI" in cname: continue
        RList = PDict[CIdx[cname]]
        sum  += RList[4]
        nct  += 1
        if RList[4]>maxval: maxval = RList[4]

    if maxval<0.001: maxval = 0
    if nct>0:
        average = float(sum)/float(nct)
    else:
        average = 0.0
    return (average,maxval)

# Calculate Average and Maximum ATS for all vendors
def getAveCompATS(PDict, CIdx):
    sum    = 0
    nct    = 0
    maxval = -99999999
    for cname in CIdx:
        if "TTI" in cname: continue
        RList = PDict[CIdx[cname]]
        sum  += RList[4]
        nct  += 1
        if RList[4]>maxval: maxval = RList[4]

    if maxval<0.001: maxval = 0
    if nct>0:
        average = float(sum)/float(nct)
    else:
        average = 0.0
    return (average,maxval)


def getMaxTTIATS(PDict):
    maxval = -99999999999
    for dk in PDict:
        SupplierCode = PDict[dk][0]
        if SupplierCode == "TTI":
            if PDict[dk][4] > maxval: maxval=PDict[dk][4]
    return maxval


#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

    #   New connection needed to retrieve data
    CONNX = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')

    TTIList = ['1.5SMC400A','1.5SMC400A','T495X226K025ATE225','SM20ML1TK6']
    MfgList = ['BOU','LTF','KEM','SOU']

    TTIList = ['TAJD337M010RNJ']
    MfgList = ['AVX']


    TTIList = ['RBR5LAM40ATR']
    MfgList = ['ROH']

    csvfile = open('./PricingCombine0.txt','r')
    outfile = open('./testHashbroken.csv','w')

    with open('data.pickle', 'rb') as f:
        PriceDataIndex = pickle.load(f)

#    outfile.write("Index,VendorCode,ManufPartNum,ATS,StdPackage,Quantity,TTI,CompAve,CompMin,CompMax,CompRange," +
#                  "CompAveATS,CompMaxATS,MinVendor\n")

    random.seed(1234)

    icnt = 0
    for ix,mpno in enumerate(TTIList):
        mfr       = MfgList[ix]
        mfrpartno = mfr+mpno


#    for mfrpartno in PriceDataIndex:

#        xr = random.random()
#        if xr<=0.40 and xr>0.50: continue

        icnt += 1
        if icnt>25000: break
        if icnt%250==0:
            print(icnt,  mfrpartno, PriceDataIndex[mfrpartno])

        PriceDict = FF_GetPriceData(csvfile, PriceDataIndex, mfrpartno, 0)

        mfr  = mfrpartno[0:3]
        mpno = mfrpartno[3:]
        PriceDict = SQL_GetPriceData(CONNX, mpno, mfr, 0)

#        for dk in PriceDict:
#          print(PriceDict[dk])
#          print(PriceDictnew[dk])
#        print(" ")

        if len(PriceDict) == 0: continue
        PR = PriceDict[1]
        VendCode = PR[1][:3]  # Get VendorCode

        #   Keep only the most current entries in Pricing Dictionary, by Supplier and PackageSize
        PriceDict = CanonPriceDictionay(PriceDict, True)
        if "D" in DEBUGLevel:
            for pr in PriceDict: print(pr, PriceDict[pr])
            print(" ")

            #   Remove Price Breaks when insufficient quantity on hand.
            #   ZeroOK -- if True, return ALL price breaks found
        PriceDict = RemoveHighPriceBreaks(PriceDict, False)
        if "Q" in DEBUGLevel:
            for pr in PriceDict: print(pr, PriceDict[pr])

            #   Get unique, sorted list of price breaks
        PBQ = GetPriceBreakPoints(PriceDict)
        if len(PBQ) == 0: continue  # Done, if no data found
        if "Q" in DEBUGLevel:  print(PBQ)  # Echo list

        #   Create Column Names
        (NumbVendors, CName) = GetColumnNames(PriceDict)  # Make a vector of column names
        if NumbVendors == 0: continue  # No competitors, skip

        if "Q" in DEBUGLevel: print(NumbVendors, CName)

        TTIIdx = GetNameIndex(PriceDict)  # Make a dictionary(colname) = idx
        #        print(TTIIdx)

        for cnm in TTIIdx:
            if "TTI" not in cnm: continue

            summa = 0.0

            #           Build Price Break Matrix
            QPMat = BuildQPMatrix(PriceDict, PBQ)
            if "M" in DEBUGLevel: print(QPMat)

            #           Add Competitor Pricing Stats
            (MinCom, XPMat) = AddStats(QPMat, PriceDict, CName, cnm)
            if "M" in DEBUGLevel: print(XPMat)

            #           Output to flat file
            xp = PriceDict[TTIIdx[cnm]]
            (nrows, ncols) = XPMat.shape
            ATS = xp[4]
            StdPckg = xp[5]

            if str(ATS) == ATS or str(StdPckg) == StdPckg:  # Error check for junk in numeric column
                print(PBQ)
                for dk in PriceDict: print(PriceDict[dk])  # Flag error
                break  # Don't output

            (AveCATS, MaxCATS) = getAveCompATS(PriceDict, TTIIdx)  # Get Average and Max Competitor ATS
            Max_TTIATS = getMaxTTIATS(PriceDict)  # Get Max TTI ATS

            if MaxCATS == 0: break  # Skip if competitors have zero inventtory

            if Max_TTIATS < max(PBQ) and MaxCATS < max(PBQ):
                print("\n", max(PBQ), ATS, MaxCATS, PBQ)
                for dk in PriceDict: print(PriceDict[dk])  # Flag error
                break  # Don't output

            sum1 = 0.0
            ave = '{:.1f}'.format(AveCATS)
            for i in range(0, nrows):
                if np.isnan(XPMat[i, 0]):
                    val = ''
                else:
                    val = '{:.4f}'.format(XPMat[i, 0])

                datastr0 = str(i + 1) + "," + VendCode + "," + mfrpartno + "," + str(ATS) + "," + \
                           str(StdPckg) + "," + str(PBQ[i]) + "," + val
                for j in range(1, ncols):
                    if np.isnan(XPMat[i, j]):
                        val = ''
                    else:
                        val = '{:.4f}'.format(XPMat[i, j])
                    datastr0 += "," + val

                MinVendor = MinCom[i][0:3]

                datastr0 = datastr0 + "," + ave + "," + str(MaxCATS) + "," + MinVendor
                outfile.write('{0:s}\n'.format(datastr0))

#                xt = []
#                for j in range(0,ncols):
#                    if np.isnan(XPMat[i,j]): continue
#                    xt.append(XPMat[i,j])
#                sum1 += sum(xt)

            summa = ATS + StdPckg

            summa += sum(PBQ) + AveCATS + MaxCATS + sum1
            print(mfrpartno+","+str(summa))
            outfile.write(mfrpartno + "," + str(summa)+"\n")

#   ALL Done
    f.close()
    outfile.close()
    csvfile.close()
    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

