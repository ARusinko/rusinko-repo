#---------------------------------------------------------------------------------------------------------------
#
#               NonLinearFit: Fit a non-linear function to smooth out pricing data.
#
#                                   A. Rusinko  (10/27/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import numpy as np
from scipy.optimize import curve_fit
from scipy import stats

__author__ = 'ARusinko'

INFile =  './testinput10272017.txt'

def func(x, a, b):
    return a*np.log(x) + b

def BuildModel(xdat,ydat):
    xlen = len(xdat)
    xd = np.zeros(xlen)
    yd = np.zeros(xlen)
    for ix,xv in enumerate(xdat):
        xd[ix] = xdat[ix]
        yd[ix] = ydat[ix]
    popt, pcov = curve_fit(func, xd, yd)
    return popt

def CalcPred(xdat, popt):
    yp = []
    for x in xdat:
        yp.append(func(x,popt[0],popt[1]))
    return yp

def WriteBack(ofile, DF, yp, CMM, ival, R2):
    xlist = []
    for ix in DF:
        (xln, iv) = DF[ix]
        if iv==ival: xlist.append(ix)
    xlist.sort()

    for ii,ix in enumerate(xlist):
        (xln, iv) = DF[ix]
        if ii==0: xln[0] = CMM
        xtmp = ','.join(map(str,xln))
        ofile.write('%s,%.3f,%.3f\n' % (xtmp,yp[ii],R2))
    return

#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):
    global INFile
    f       = open(INFile,'r')
    outfile = open("./testout.csv", 'w')

    icnt = 0
    ival = 0
    DFil = {}
    Commodity = ''
    Category  = ''

    for line in f:
        icnt += 1
        if icnt==1:
            zline = line.splitlines()
            xline = zline[0].split('\t')
            xline.append("Pred25%")
            xline.append("R^2")
            outfile.write(",".join(xline)+"\n")
            continue                                # Header Line

        zline = line.splitlines()
        xline = zline[0].split('\t')
        if float(xline[3])<1: xline[3]=1.00

        # New Category Model
        if len(xline[1])>0:
            if icnt>2:
                print('\n',Commodity,Category)
                popt  = BuildModel(xdat,ydat)
                ypred = CalcPred(xdat, popt)
                (RSq, b_s, r, tt, stderr) = stats.linregress(ydat,ypred)
                print('yp = %.3f*ln(x) + %.3f [r^2=%.3f]' % (popt[0],popt[1],RSq))
                WriteBack(outfile, DFil, ypred, Commodity, ival, RSq)
                ival+=1

            Category  = xline[1]
            if len(xline[0]) > 0: Commodity = xline[0]  # New Commodity Label
            xdat = []
            ydat = []
            xdat.append(float(xline[3]))
            ydat.append(float(xline[5]))
            DFil[icnt] = (xline,ival)
            continue

        xdat.append(float(xline[3]))
        ydat.append(float(xline[5]))
        DFil[icnt] = (xline, ival)

    f.close()
    outfile.close()
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

