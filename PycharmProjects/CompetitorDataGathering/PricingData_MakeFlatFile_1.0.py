#---------------------------------------------------------------------------------------------------------------
#
#           PricingDataTiming: Gather TTI pricing data from SQL Server and construct a matrix of
#                        quantity/price break information.
#
#                                   A. Rusinko  (10/20/2017)
#---------------------------------------------------------------------------------------------------------------
import sys
import pyodbc
import numpy as np
import time
import random

__author__ = 'ARusinko'

DEBUGLevel = "N"                            # Levels = 'N,D,Q,M'

# SQL_GetCompPriceData By Manufacturer Part Number, reformat and return dictionary of relevant data
def SQL_GetPriceData(CONNX, TestPartNum, Manufacturer, minATS=0 ):
    SCodes = {
    'www.ttiinc.com':'TTI','www.peigenesis.com':'PEI','www.newark.com':'NRK',
    'www.futureelectronics.com':'FUT','www.digikey.com':'DKY','www.arrow.com':'ARW',
    'avnetexpress.avnet.com':'AVN', 'estore.heilind.com':'HEI' }

    try:
        cursor = CONNX.cursor()
    except AttributeError:
        print("\n\n ERROR! No database connection established.")
        sys.exit(1)

    SQLquery = """ SELECT *
                FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] \
                WHERE [MfrPartNumber] = ? And [Mfg] = ? And [ATS] >= ? """

    cursor.execute(SQLquery,(TestPartNum, Manufacturer, minATS))       # SQL Query Execution

    PrDict = {}
    ix = 0
    for row in cursor:
        if "D" in DEBUGLevel: print(row)
        RList  = []                                     # Create List for each record
        RList.append(SCodes[row[0]])                    # Manufacturer Code
        RList.append(row[1]+row[2])                     # Manufacturer Part Number
        RList.append(fixSQLdate(row[3]))                # Date Stamp
        RList.append(row[-1])                           # Package Type
        RList.append(row[4])                            # ATS

        if row[6] == None: continue                     # Skip if NO price break info available

        if row[5] == None:                              # Minimum Package Size
            RList.append(row[6])                        # Use first price break if NULL
        else:
            RList.append(row[5])                        # Use provided value

#       Add price breaks (quantity,price) to list
        i = 6
        while (i < len(row)-1):                         # Price break tuples (Quantity,Price)
            if RList[0]=="TTI":
                if row[i] == 0: break                   # Break out if quantity=0 found
                rval = float(row[i + 1])
                if rval < 0.00001: break                # Break out if price=0.0 found
                tupval = (row[i], rval)
                RList.append(tupval)
            else:
                if row[i] == None: break
                tupval = (row[i], float(row[i + 1]))
                RList.append(tupval)
            i += 2

        ix += 1
        PrDict[ix] = RList                              # Store in PrDict
    return PrDict                                       # Return Dictionary -- PrDict

# Date Fix
def fixSQLdate(dateval):
    nd = str(dateval).split(" ")
    return nd[0]


#----------------------------------------------------------------------------------------------------
#		                                    MAIN Code
#----------------------------------------------------------------------------------------------------
def main(argv):

    start = time.time()

#    TTIList = ['SM20ML1TK6']
#    MfgList = ['SOU']

    TTIList = ['T495X226K025ATE225']
    MfgList = ['KEM']

    TTIList = ['1.5SMC400A','1.5SMC400A']
    MfgList = ['BOU','LTF']

    #    f = open('C:/Users/arusinko/Documents/TTI Stuff/PriceComparison.csv','w')
    f = open('./TestDat0.txt','w')


    CONN1 = pyodbc.connect('Driver={SQL Server};'
                       'Server=TXSQLD08\TXSQLD08;'
                       'Port=1433;'
                       'Database=PricingAnalytics;'
                       'Trusted_Connection=yes;')
    PartsList = CONN1.cursor()                                          # Get list of partnos and mfg
    SQLquery = """ SELECT DISTINCT [MfrPartNumber], [mfg]
            FROM [PricingAnalytics].[dbo].[PRICING_COMBINE] """
    PartsList.execute(SQLquery)

#   New connection needed to retrieve data
    CONNX = pyodbc.connect('Driver={SQL Server};'
                           'Server=TXSQLD08\TXSQLD08;'
                           'Port=1433;'
                           'Database=PricingAnalytics;'
                           'Trusted_Connection=yes;')
    icnt = 0
    for row in PartsList:

        xrnd = random.random()
#        if xrnd <0.63 or xrnd>=0.73: continue

        mfrpartno = row[0]                          # Find part by Mfg Part Number and Mfg
        mfg       = row[1]

#    for ibx, mfrpartno in enumerate(TTIList):
#        mfg = MfgList[ibx]

        if "," in mfrpartno: continue               # CODE AROUND Comma in ManPartNo

#       Record count
        icnt+=1
#        if icnt >25: break
        if icnt%100==0:
            print(icnt, mfrpartno)

#   Get a dictionary containing lists of data by MnfPartNo and minimum ATS
#   minATS > 0 exlcudes any record with no stock on hand.
        PriceDict = SQL_GetPriceData(CONNX, mfrpartno, mfg, 0)

        if len(PriceDict)==0: continue
        PR       = PriceDict[1]
        f.write(PR[1] + "\tCur\n")
        for dk in PriceDict:
#            print(dk,PriceDict[dk])
            s0 = "\t".join(map(str,PriceDict[dk]))
            f.write(s0+"\n")
        f.write("$$$$\n")

    f.close()
    CONN1.close()
    CONNX.close()

    end = time.time()
    timed = '{:.1f}'.format(end-start)
    print("\n\n",icnt,"Processed ---- Elasped:",timed,'s')
    pass

# EXECUTE Main
if __name__ == "__main__":
        main(sys.argv)

